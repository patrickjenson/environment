#!/bin/zsh

# See for inspiration: https://gist.github.com/claui/ada85e696029cfa8cba9b91723ce2e5b

# Using a brace block to prevent running an incomplete file.
{

setopt ERR_EXIT NO_UNSET PIPE_FAIL

if [[ 'Darwin' != "$(uname -s)" ]]; then
	echo "\n\tThis script is only useful on macOS.\n" >&2
	exit 1
fi

if [[ 0 -ne "${EUID:-}" ]]; then
	echo "\n\tSetup can only be done as root.\n" >&2
	exit 1
fi

brew_install_folder='/opt/homebrew'
brew_bin_dir="${brew_install_folder}/bin"
brew_binary="${brew_bin_dir}/brew"
brew_real_name='Homebrew'

# Ensure that PATH is sane before running anything.
# $brew_bin_dir shouldn't exist yet, but this makes a few steps below easier.
path=("${brew_bin_dir}" '/usr/local/bin' '/usr/bin' '/bin' '/usr/local/sbin' '/usr/sbin' '/sbin')

# Set this to anything else to disable the message about
# 	allowing the terminal to administer the system.
: ${SETTINGS_MESSAGE:=1}

# Display message about System Settings and allow the user to change the needed ones.
if [[ ! -f "${brew_binary}" && 1 -eq "${SETTINGS_MESSAGE}" ]]; then
	echo "\n\t${brew_real_name} needs to have access to modify applications."
	echo "\n\tTo do this:"
	echo "\t\tadd Terminal (and any other similar applications) to"
	echo "\t\tthe list in 'System Settings' -> 'Privacy & Security' -> 'App Management'."
	echo "\n\tNote that some setup steps require additional (temporary) permissions."
	echo "\tEither allow access when the pop-up appears or"
	echo "\t\tadd Terminal to the list in 'Privacy & Security' -> 'Full Disk Access'."
	echo "\n\tNote also that adding an application to the above lists usually requires re-launching it."
	echo -n "\n\tOpen 'Privacy & Security' now? [Y/n] "
	read -r
	if [[ 'n' != "${REPLY}" && 'N' != "${REPLY}" ]]; then
		open '/System/Library/PreferencePanes/Security.prefPane'
		unset REPLY
		echo -n "\n\tPress Return to continue installing ${brew_real_name}..."
		while [[ $'\n' != "${REPLY:-}" ]]; do
			read -rsk1
		done
	fi
fi
echo

function get_gid() {
	# Fetch the GID of a group.
	local ds_entry="$(dscl . -read /Groups/${1:-} 2>/dev/null || true)"
	echo "$(sed -rn 's%^PrimaryGroupID: ([0-9]+)$%\1%p' <<< ${ds_entry})"
}

# Role accounts are required to start with an '_'.
brew_account_name='_brew'

# Create group if not exists.
echo -n "Creating ${brew_real_name} management group => "
if [[ -z "$(get_gid ${brew_account_name})" ]]; then
	dseditgroup -o create -r "${brew_real_name}" "${brew_account_name}"
	dseditgroup -o edit -a 'root' -t user "${brew_account_name}"
	echo "COMPLETE"
else
	echo "ALREADY CREATED"
fi

# The account default group currently expected by homebrew.
default_group='staff'

brew_home='/var/brew'

# Create service account if not exists.
echo -n "Creating ${brew_real_name} management account => "
if [[ -z "$(dscl . -read /Users/${brew_account_name} 2>/dev/null || true)" ]]; then
	# This _must_ be in range [200-400] for a role account.
	# This is an arbitrary choice that is unlikely to already be in use.
	brew_uid=350
	
	new_user_params=()
	new_user_params+=('-addUser' "${brew_account_name}")
	new_user_params+=('-fullName' "${brew_real_name}")
	new_user_params+=('-UID' "${brew_uid}")
	new_user_params+=('-GID' "$(get_gid ${default_group})")
	new_user_params+=('-admin') # The Homebrew account, currently, _must_ be an admin to install Casks.
	
	# Currently 'Homebrew/brew/package/scripts/postinstall' tries to write into
	# 	<HOME_OF_HOMEBREW_PKG_USER>/..., which will not work for role accounts.
	# The 'home' folder for role accounts is '/var/empty' which is not writable by not-root.
	# A role account would be preferred, but the above script needs to change first.
	# There also seem to be parts of the Homebrew system that are not compatible with a role account,
	# 	so the below steps will be used instead.
	# new_user_params+=('-roleAccount')
	
	# Prevent this account form logging in directly.
	new_user_params+=('-shell' '/usr/bin/false')
	
	# Use a specific location for the homebrew account home folder.
	new_user_params+=('-home' "${brew_home}")
	# This handles creating the main folder and one that homebrew needs to exist for logging.
	mkdir -p "${brew_home}/Library/logs"
	
	# Configure the default shell session.
	rc_file="${brew_home}/.zshenv"
	touch "${rc_file}"
	chmod 0640 "${rc_file}"
	cat > "${rc_file}" <<-EOF
	#!/bin/zsh
	
	unsetopt GLOBAL_RCS
	unsetopt BEEP
	
	setopt INTERACTIVE_COMMENTS
	setopt PROMPT_SUBST
	
	unset LD_LIBRARY_PATH
	unset LD_PRELOAD
	
	typeset -g KEYTIMEOUT=5
	typeset -g SHELL_SESSIONS_DISABLE=1
	
	typeset -gx HOMEBREW_NO_ANALYTICS=1
	typeset -gx HOMEBREW_NO_EMOJI=1
	typeset -gx HOMEBREW_NO_ENV_HINTS=1
	
	typeset -gx MANPAGER='less -+X -iRx4'
	typeset -gx PAGER="\${MANPAGER}"
	alias less="\${MANPAGER}"
	alias cls='clear'
	
	umask 0022
	
	NEWLINE=\$'\\n'
	PROMPT='\${NEWLINE}brew> %~%(1j. jobs=%j.) %?\${NEWLINE}%# %f'
	
	# This _must_ be done before any 'brew' commands
	# 	because the PWD doesn't carry over into the Sudo session;
	# this ensures a known real directory is used.
	cd ~
	
	PATH='${PATH}'
	
	rm -rf ~/.zsh_sessions/
	
	fpath+=("\$(brew --prefix)/share/zsh/site-functions")
	autoload -Uz compinit && compinit
	autoload -Uz bashcompinit && bashcompinit
EOF
	
	sysadminctl "${new_user_params[@]}" &>/dev/null
	dseditgroup -o edit -a "${brew_account_name}" -t user "${brew_account_name}"
	chown -R "${brew_account_name}:${default_group}" "${brew_home}"
	find "${brew_home}" -type d | xargs -n1 chmod 2750
	echo "COMPLETE"
else
	echo "ALREADY CREATED"
fi

# Ensure that current user is a member of the above group.
if [[ -n "${SUDO_USER:-}" ]]; then
	echo -n "Adding current super-user (${SUDO_USER}) to ${brew_real_name} management group => "
	if [[ -z "$(groups ${SUDO_USER} | command grep ${brew_account_name})" ]]; then
		dseditgroup -o edit -a "${SUDO_USER}" -t user "${brew_account_name}"
		echo "COMPLETE"
	else
		echo "ALREADY CONFIGURED"
	fi
else
	echo "\nUnable to determine current super-user." >&2
	echo "Each user that should be able to control ${brew_real_name}" >&2
	echo "\tneeds to be added to the (${brew_account_name}) group." >&2
	echo "Eg. $> sudo dseditgroup -o edit -a '<USER_NAME>' -t user ${brew_account_name}" >&2
	echo
fi

# Ensure that members of above group are able to interact with homebrew.
sudo_file='/etc/sudoers.d/brew'
echo -n "Enabling members of ${brew_real_name} group to manage installation => "
if [[ ! -e "${sudo_file}" ]]; then
	# The requirement to use sudo makes it impossible to execute homebrew directly and have it work;
	# 	see below `useful commands` section for a workaround.
	# The main reason for this is that the current directory doesn't get mapped correctly under sudo.
	# The environment provided above puts the shell session in a known state so the tools work.
	echo "Defaults:root secure_path=\"${PATH}\"" > "${sudo_file}"
	echo "Defaults:${brew_account_name} !authenticate" >> "${sudo_file}"
	echo "%${brew_account_name} ALL = (${brew_account_name}) SETENV: /bin/zsh" >> "${sudo_file}"
	
	chown root:wheel "${sudo_file}"
	chmod 440 "${sudo_file}"
	visudo -csOP -f "${sudo_file}" >/dev/null
	echo "COMPLETE"
else
	echo "ALREADY CONFIGURED"
fi

# ~ macOS Sequoia (15.0) throws SIGBUS when trying to set this value; reevaluate adding this back at a later date.
# # Allow system applications to see and use the homebrew installed tools if nothing else provides X.
# current_path="$(launchctl getenv PATH)"
# echo -n "Enabling system applications to use ${brew_real_name} tools => "
# if [[ -z "$(command grep homebrew <<< ${current_path})" ]]; then
# 	launchctl config user path "${current_path:-${PATH}}" >/dev/null
# 	echo "COMPLETE"
# else
# 	echo "ALREADY CONFIGURED"
# fi

# Ensure that Rosetta 2 is installed.
echo -n "Ensuring that Rosetta 2 is installed => "
([[ -n "$(pgrep oahd)" ]] && arch -x86_64 '/usr/bin/true' && echo "ALREADY INSTALLED") || (
	# There is a known issue where this may look like it fails, but actually works correctly.
	(sleep 2; echo) | softwareupdate --install-rosetta --agree-to-license &>/dev/null || true
	echo "COMPLETE"
)

# Ensure that XCode CLTs are installed.
# 'xcode-select --install' currently requires using a gui, so using below method instead.
clt_home='/Library/Developer/CommandLineTools'
echo -n "Ensuring that XCode CommandLineTools are installed => "
if [[ ! -d "${clt_home}" ]]; then
	clt_progress='/tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress'
	touch "${clt_progress}"
	softwareupdate --install "$(
		softwareupdate --list 2>/dev/null | \
		sed -rn 's%\* Label: (.*)$%\1%p' | \
		command grep 'Command Line Tools' | tail -n1
	)" &>/dev/null
	rm "${clt_progress}"
	echo "COMPLETE"
else
	echo "ALREADY INSTALLED"
fi

# Ensure that XCode CLTs are accessible.
if [[ "${clt_home}" != "$(xcode-select --print-path)" ]]; then
	xcode-select --switch "${clt_home}"
fi

# Install homebrew if not exists.
echo -n "Installing ${brew_real_name} => "
if [[ ! -f "${brew_binary}" ]]; then
	# Inform the .pkg installer which account should be the owner of the install root.
	defaults write /var/tmp/.homebrew_pkg_user HOMEBREW_PKG_USER "${brew_account_name}"
	
	# Fetch and install the latest _packaged_ version of homebrew.
	asset_list='https://api.github.com/repos/Homebrew/brew/releases'
	latest_pkg="$(curl -fsSL ${asset_list} | sed -rn 's%^.*"(.*pkg)"$%\1%p' | head -n1)"
	pkg_file="/tmp/$(basename ${latest_pkg})"
	curl -fsSL -o "${pkg_file}" "${latest_pkg}"
	installer -package "${pkg_file}" -target '/' >/dev/null
	rm "${pkg_file}"
	
	# Initialize the homebrew system.
	init_commands=('cd ~') # Ensure pwd is a known value.
	init_commands+=("${brew_binary} analytics off") # Disable anaylitics.
	init_commands+=("${brew_binary} update") # Get the latest pkg repo listing.
	sudo -HEu "${brew_account_name}" /bin/zsh -c "${(j%;%)init_commands[@]}" &>/dev/null
	
	# Cleanup the install artifact permissions.
	chown -R "${brew_account_name}:${default_group}" "${brew_home}"
	find "${brew_home}" -type d | xargs -n1 chmod 2750
	
	# Cleanup the install root.
	chown -R "${brew_account_name}:${default_group}" "${brew_install_folder}"
	find "${brew_install_folder}" -type d | xargs -n1 chmod 2755
	echo "COMPLETE"
else
	echo "ALREADY INSTALLED"
fi

# One of the above processes creates this file and it needs to be removed.
if [[ -n "$(find ${PWD} -maxdepth 1 -user root -name '.gitconfig')" ]]; then
	rm "${PWD}/.gitconfig"
fi

echo "\n${brew_real_name} was successfully installed!"

echo "\n\tHere are a selection of useful commands to add to '~/.zshrc':\n"
echo "\teval \"\$(${brew_binary} shellenv zsh)\""
echo "\tfunction brew_cmd() {"
echo "\t\tlocal brew_account_name='${brew_account_name}'"
echo "\t\tif [[ 0 -eq \${#} ]]; then"
echo "\t\t\t# This will start a shell that can interact with Homebrew."
echo "\t\t\tsudo -HEsu \${brew_account_name}"
echo "\t\telse"
echo "\t\t\tlocal cmd=\"${brew_binary} \${@}\""
echo "\t\t\tsudo -HEu \${brew_account_name} /bin/zsh -c \"cd ~; \${cmd}\""
echo "\t\tfi"
echo "\t}"
echo

exit 0

}

#!/bin/zsh

setopt ERR_EXIT NO_UNSET PIPE_FAIL

# These _must_ exactly match the values from 'macos/setup.sh'.
brew_install_folder='/opt/homebrew'
brew_bin_dir="${brew_install_folder}/bin"
brew_binary="${brew_bin_dir}/brew"
brew_account_name='_brew'

if [[ 0 -eq ${#} ]]; then
	# This will start a shell that can interact with Homebrew.
	sudo -HEsu "${brew_account_name}"
else
	sudo -HEu "${brew_account_name}" /bin/zsh -c "cd ~; ${brew_binary} ${*}"
fi

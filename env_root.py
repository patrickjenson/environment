#!/usr/bin/env python3

'''A script to manage a host system configuration.'''

# pylint: disable=too-many-lines(C0302)
# pyright: reportPossiblyUnboundVariable=false

from argparse import ArgumentParser
from collections.abc import Sequence
from os import devnull, environ
from pathlib import Path
from platform import machine
from shutil import rmtree, which
from subprocess import PIPE, Popen, run
from sys import argv, exit as sys_exit, stderr, stdout
from typing import TextIO

from distro_util import DistroFamily, SessionFamily, distro_name as get_distro_name

# ? What is the umask here? Does it need changing?

_HOST_MACHINE = machine()
_DISTRO_NAME = get_distro_name()
_DISTRO_FAMILY = DistroFamily.fetch()
_X64_HOST = _HOST_MACHINE == 'x86_64'
_RUNNING_GUI = SessionFamily.is_running_gui()

if _DISTRO_FAMILY is DistroFamily.MACOS:
	environ['HOMEBREW_NO_ANALYTICS'] = '1'
	environ['HOMEBREW_NO_EMOJI'] = '1'
	environ['HOMEBREW_NO_ENV_HINTS'] = '1'

_RUNNING_DRY = False
_RUNNING_QUIET = False

def log(message: str, /, output_file: TextIO = stdout) -> None:
	'''Print a message to output_file if not _RUNNING_QUIET.'''
	
	if not _RUNNING_QUIET and output_file is not devnull:
		print(message, file=output_file)

try:
	from requests import get, RequestException
except ModuleNotFoundError:
	log('\tRequests library is unavailable; Try installing \'py[thon]{{3,}}-requests\'.\n', stderr)
	sys_exit(1)

_PREFERED_LOCALE = 'en_US.UTF-8'
_PPA_LIST: list[str] | None = None

def get_ppa_list() -> None:
	'''Fetch a list of enabled ppa repos for this system.'''
	
	# pylint: disable=global-statement(W0603)
	
	from subprocess import CalledProcessError
	
	global _PPA_LIST # noqa: PLW0603
	
	ppa_cmd = ['grep', '-hrP']
	# ppa_cmd += [r'--include=*.list']
	ppa_cmd += [r'^deb .*ppa\.launchpadcontent\.net.*$']
	ppa_cmd += ['/etc/apt/sources.list.d/']
	
	_PPA_LIST = []
	try:
		ppa_result = run(ppa_cmd, capture_output=True, check=True, encoding='utf-8')
	except CalledProcessError:
		return
	
	for repo_line in ppa_result.stdout.split('\n')[:-1]:
		_PPA_LIST.append('/'.join(repo_line.split()[1].split('/')[-4:-2]))

def get_env_dir() -> Path:
	'''Get the location of the environment repo.'''
	
	from os.path import normpath
	
	path = Path(__file__)
	if path.is_symlink():
		path = path.readlink()
	return Path(normpath(str(path.parent)))

def add_ppa(repo: str, /) -> None:
	'''Enable ppa, if able.'''
	
	if _PPA_LIST is not None and repo not in _PPA_LIST:
		log(f"\t     run: add-apt-repo ppa:{repo}")
		if not _RUNNING_DRY:
			_ = run(['add-apt-repository', '-y', f"ppa:{repo}"], stdout=stdout, check=True)
			get_ppa_list()

def configure_dnf(parallel_jobs: int = 8, /) -> None:
	'''Change dnf configuration, if needed.'''
	
	dnf_file = Path('/etc/dnf/dnf.conf')
	
	if dnf_file.exists():
		log(f"\t  enable: dnf parallel package downloads ({parallel_jobs})")
		if not _RUNNING_DRY:
			new_content = []
			
			with dnf_file.open('r', encoding='utf-8') as content:
				for line in content.readlines():
					if line.find('max_parallel_downloads') == -1:
						new_content.append(line)
				
				new_content.append(f"max_parallel_downloads={parallel_jobs}\n")
			
			with dnf_file.open('w', encoding='utf-8') as content:
				content.writelines(new_content)

def enable_services() -> None:
	'''
	Enable services for this system.
	Most systems will not need this; services are usually enabled on install (and reboot).
	'''
	
	from distro_util import InitFamily
	
	# See: https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet for openrc hints.
	
	services = {
		DistroFamily.ALPINE: [
			'fail2ban',
			'haveged',
			'rngd',
		],
	}
	
	init_system = InitFamily.fetch()
	
	for service in services.get(_DISTRO_FAMILY, []):
		enable_cmd = init_system.get_enable_service_cmd(service)
		log(f"\t  enable: {' '.join(enable_cmd)}")
		if not _RUNNING_DRY:
			_ = run(enable_cmd, check=True)
		
		if init_system is not InitFamily.RUNIT:
			restart_cmd = init_system.get_restart_service_cmd(service)
			log(f"\t restart: {' '.join(restart_cmd)}")
			if not _RUNNING_DRY:
				_ = run(restart_cmd, check=True)

def enable_nodesource() -> bool:
	'''Enable the NodeSource repo for this installation.'''
	
	# pylint: disable=consider-using-with(R1732)
	
	# NodeSource does support rpm, but this only seems to be needed for Debian-family.
	if _DISTRO_FAMILY is not DistroFamily.DEBIAN:
		return False
	
	# See: https://github.com/nodesource/distributions for details.
	if not Path('/etc/apt/sources.list.d/nodesource.list').exists():
		curl_cmd = ['curl', '-fsSL', 'https://deb.nodesource.com/setup_lts.x']
		shell = 'bash' # This script does not seem to be zsh compatible.
		
		log(f"\t     run: {' '.join(curl_cmd)} | {shell}")
		if not _RUNNING_DRY:
			curl_result = Popen(curl_cmd, stdout=PIPE)
			_ = run([shell, '-s'], stdin=curl_result.stdout, stdout=stdout, check=True)
			
			if curl_result.stdout is not None: # silence a mypy error
				curl_result.stdout.close() # forwards a SIGPIPE to the shell if needed.
	
	return True

def get_packages_for_system(
	*,
	include_arduino: bool = False,
	include_copyq: bool = False,
) -> list[str]:
	'''Get a list of basic packages needed for this system.'''
	
	# pylint: disable=too-many-branches(R0912)
	
	from platform import release
	
	# All distros need these.
	packages = [
		'asciidoc',
		'automake',
		'bat',
		'jq',
		'luajit',
		'luarocks',
		'ripgrep',
		'sd',
		'tmux',
		'vim',
		# 'stow',
		# 'qemu',
	]
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS:
		packages.extend([
			'clang',
			'virt-what',
			# 'ufw',
		])
	
	if _DISTRO_FAMILY is not DistroFamily.DEBIAN:
		packages.extend([
			'eza', # Debian 12 doesn't have this, and Ubuntu has an incomplete version.
			'hexyl', # Neither Debian nor Ubuntu have a package for this.
		])
	
	kernel_release = release()
	
	if _DISTRO_FAMILY is DistroFamily.DEBIAN:
		if _DISTRO_NAME != 'ubuntu' and not _X64_HOST:
			packages.append('raspberrypi-kernel-headers')
		else:
			packages.append(f"linux-headers-{kernel_release}")
		
		packages.extend([
			'asciidoctor',
			'autoconf',
			'build-essential',
			'clangd',
			'cmake', # See other packages 'cmake-*'.
			'default-jre',
			'doas',
			'duf',
			'fail2ban',
			'fd-find',
			'flatpak',
			'fuse3',
			'git', # newer from ppa
			'git-doc',
			'git-man',
			'global',
			'golang',
			'haveged',
			'libfontconfig-dev',
			'libfuse3-dev',
			'libgmp-dev',
			# 'liblua5.4-dev'
			'libpcre3-dev',
			'libusb-1.0-0-dev',
			'libxcb-composite0-dev',
			'libxkbcommon-dev',
			'libtool',
			'lua5.1',
			# 'lua5.4',
			'pandoc',
			'pkgconf',
			'python3-doc',
			'python3-pip',
			'python3-requests',
			'python3-venv',
			'rng-tools5',
			'scdoc',
			'shellcheck',
			'universal-ctags',
			'vim-doc',
			'zsh-static',
			# 'environment-modules',
			# 'openssh-server',
		])
		
		if _DISTRO_NAME != 'ubuntu':
			packages.extend([
				'libusb-1.0-0',
				'liblua5.1-0-dev',
				'p7zip-full',
			])
		else:
			packages.extend([
				'7zip',
				'liblua5.1-dev',
				'openjdk-8-jdk', # Debian dropped support for this at some point.
				'tree-sitter-cli',
			])
		
		if enable_nodesource():
			packages.append('nodejs')
		else:
			packages.append('npm')
		
		if _X64_HOST:
			packages.append('crossbuild-essential-arm64')
		
		if include_copyq:
			packages.extend([
				'copyq',
				'copyq-doc',
				'copyq-plugins',
			])
		
		if include_arduino:
			packages.append('arduino')
	
	elif _DISTRO_FAMILY is DistroFamily.REDHAT:
		packages.extend([
			'kernel-headers',
			'rubygem-asciidoctor',
			'autoconf',
			'cmake', # See other packages 'cmake-*'.
			'ctags',
			'duf',
			'flatpak',
			'gcc-c++',
			'git',
			'git-delta',
			'global-ctags',
			'golang',
			'haveged',
			'fail2ban',
			'fd-find',
			'fontconfig-devel',
			'fuse3-devel',
			'java-1.8.0-openjdk-devel',
			'libxcb-devel',
			'libtool',
			'libusb1-devel',
			# 'lua-devel', # No 5.1* available.
			'nodejs',
			'nodejs-npm',
			'opendoas',
			'p7zip',
			'pandoc',
			'pcre-devel',
			'perl',
			'python3-docs',
			'python3-pip',
			'python3-requests',
			'readline-devel',
			'redhat-lsb-core',
			'rng-tools',
			'ShellCheck', # Fedora, why is this case-sensitive!?
			'tree-sitter-cli',
			'vim-enhanced',
			'zsh',
		])
		
		if _X64_HOST:
			packages.append('gcc-c++-aarch64-linux-gnu')
		
		if include_copyq:
			packages.append('copyq')
		
		if include_arduino:
			packages.extend([
				'avr-gcc-c++',
				'avrdude',
			])
	
	elif _DISTRO_FAMILY is DistroFamily.ALPINE:
		# ~To publish hostname to dns:
		# ~	'doas apk add dbus dbus-openrc avahi avahi-openrc';
		# ~	'doas rc-update add avahi-dnsconfd default';
		# ~	'doas rc-service avahi-dnsconfd start'
		
		# See: https://wiki.alpinelinux.org/wiki/Desktop_environments_and_Window_managers
		
		packages.extend([
			'docs', # meta-package to install `X-doc`, when available and installing 'X'
			'pyc', # meta-package to install precompiled python modules
			'linux-headers',
			'build-base',
			'7zip',
			'asciidoctor',
			'autoconf',
			'bash-completion',
			'binutils-gold',
			'choose',
			'clang-extra-tools',
			'cmake',
			'coreutils',
			'ctags',
			'delta',
			'fail2ban',
			'fd',
			'flatpak',
			'fontconfig',
			'fuse3',
			'fuse3-dev',
			'git',
			'gmp-dev',
			'gnupg-gpgconf',
			'go',
			'grep',
			'haveged',
			'less', # GNU less, instead of busybox
			'libffi-dev',
			'libtool',
			'libusb-dev',
			'lua5.1-dev',
			# 'lua5.4-dev',
			'man-pages-posix',
			'musl-locales',
			'ncurses',
			'ncurses-dev',
			'npm',
			'openjdk8',
			'pandoc-cli',
			'pcre-dev',
			'pkgconf',
			'py3-pip',
			'py3-requests',
			'rng-tools',
			'shadow',
			'shellcheck',
			'tar', # GNU tar, instead of busybox
			'tree-sitter-cli',
			'xz-dev',
			'zsh',
		])
		
		if _X64_HOST:
			packages.append('gcc-aarch64-none-elf')
		
		# ~ only available in 'edge'.
		# if include_copyq:
		# 	packages.append('copyq')
		
		if include_arduino:
			packages.extend([
				'gcc-avr',
				'avrdude',
			])
	
	elif _DISTRO_FAMILY is DistroFamily.MACOS:
		packages.extend([
			# To have the GNU version(s) available.
			'coreutils',
			'findutils',
			'gnu-sed',
			'grep',
			'proctools',
			
			'asciidoctor',
			'choose-rust',
			'cmake', # The Cask is _not_ preferred.
			# 'dbus', # ? Needed? # ~ needs `brew services start dbus`
			'duf',
			'fd',
			'git', # Using this to get a newer copy vs. XCode CLTs.
			'git-delta',
			'global',
			'go',
			'gpgme',
			'libtool',
			'libusb',
			# 'lua', # No lua5.1* available.
			'node',
			'openjdk', # ~ See below for extra step.
			'pandoc',
			'pkg-config',
			'python3',
			'rust',
			'sevenzip',
			'shellcheck',
			'tree-sitter',
			'universal-ctags',
			'vivid',
			'wget',
			# 'macos-trash',
			# 'mas',
			# 'modules',
			# 'qemu',
			# 'stow',
		])
		
		# ~ See: https://www.java.com/en/download/manual.jsp
		# ~ 	for other versions of the JRE. Eg. 8u431
		
		system_location = Path('/Library/Java/JavaVirtualMachines/openjdk.jdk')
		if not _RUNNING_DRY and not system_location.exists() and not system_location.is_symlink():
			jdk_location = '/opt/homebrew/opt/openjdk/libexec/openjdk.jdk'
			system_location.symlink_to(jdk_location)
		
		# The above list excludes the following package for the listed reasons:
		# 	antlr -> existing installation method is preferred,
		# 	curl -> macOS provides it sown copy which should be user instead,
		# 	docker -> the Cask is preferred as it provides Desktop, not just the engine,
		# 	fail2ban -> only needed on publicly visible machines; why not just run a Linux server?,
		# 	gcc -> a version comes with XCode CLTs which should be used instead,
		# 	macvim -> to stay with known system and start moving to NeoVim instead,
		# 	openssh -> macOS provides its own system which should be used instead,
		# 	zsh ->
		# 		The system zsh is new enough, but does not come with docs or completion scripts.
		# 		The issue is that compaudit will not allow these scripts to load bcs
		# 			they are not owned by (USER | root).
		# 		The ones bundled with tools have to be copied to a `safe` folder to be used.
		
		# The following packages are not installed above to maintain consistency with
		# 	other environments where they are installed into `$HOME/<somewhere>`,
		# 	but there is nothing preventing their installation
		# 	given tweaks in the environemnt to find them.
		# 	direnv
		# 	ghcup
		# 	minikube
		# 	zsh-syntax-highlighting
		
		# Here are desired apps that come from the App Store, their IDs for mas, and prices:
		# 	Pages [409201541]
		# 	Numbers [409203825]
		# 	Keynote [409183694]
		# 	Swift Playgrounds [1496833156]
		# 	UTM [1538878817] ($9.99); fetching via Cask
		# 	Mic-Drop [1489816366] ($4.99, % goes to charity); fetching via Cask
		
		# CopyQ is handled by a Cask.
		# The ARM toolchain is installed with the XCode CLTs.
		
		if include_arduino:
			packages.append('arduino-cli')
	
	return packages

def get_gui_packages() -> list[str]:
	'''Get a list of gui packages needed for this system, if a gui is running.'''
	
	session_type = SessionFamily.fetch()
	
	if session_type is SessionFamily.MACOS:
		# macOS will always have a GUI running.
		return ['git-gui', 'pinentry-mac']
	
	if session_type is SessionFamily.TTY:
		return []
	
	packages = [
		'gnome-tweaks', # ~ Update this if/when using another DE.
		'meld',
	]
	
	if session_type is SessionFamily.X11:
		packages.extend([
			'xclip',
			'xsel',
		])
	
	elif session_type is SessionFamily.WAYLAND:
		packages.extend([
			'wl-clipboard',
		])
	
	if _DISTRO_FAMILY is DistroFamily.DEBIAN:
		packages.extend([
			# 'gnome-shell-extension-manager', # using flatpak instead.
			'vlc',
			# 'gufw',
		])
		
		if _DISTRO_NAME == 'ubuntu':
			packages.extend([
				'gitk',
				'vim-gtk3',
				'zsh-doc',
			])
	
	elif _DISTRO_FAMILY is DistroFamily.REDHAT:
		packages.extend([
			'alacritty',
			'gitk',
			'python3-docs',
			'vim-X11',
			'zsh-html',
			# 'vlc', # RPMFusion
		])
	
	elif _DISTRO_FAMILY is DistroFamily.ALPINE:
		packages.extend([
			'alacritty',
			'alacritty-zsh-completion',
			'vlc',
			'git-gitk',
			'git-gui',
			'gvim',
		])
	
	return packages

def install_casks(
	extras: list[str] | None = None,
	*,
	include_arduino: bool = False,
	include_copyq: bool = False,
	**_kwargs, # This is to catch any unnamed, unneeded kwargs.
) -> None:
	'''Install applications that only come in Cask form.'''
	
	from re import split
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS:
		log(f"\t  refuse: Homebrew Casks are not supported on {_DISTRO_NAME} at this time", stderr)
		return
	
	# UTM stores the IPSW files in ~/Library/Containers/com.utmapp.UTM/Data/Library/Caches.
	
	packages = [
		'alacritty',
		# 'cmake', # The non-Cask is preferred.
		'google-chrome',
		# 'handbrake',
		# 'mic-drop',
		# 'neovide',
		'utm',
		'vlc',
		# 'vmware-fusion',
		
		'font-symbols-only-nerd-font',
		
		# 'firefox',
		# 'imageoptim',
		# 'linearmouse',
		# 'kitty',
		# 'macfuse', # requires kext approval; see Caveat section for instructions.
		# 'meld',
		# 'rectangle', # macOS Sequoia window tiling seems to obviate this.
		# 'unclack',
		
		# See: https://docs.docker.com/desktop/mac/permission-requirements/ for details.
		# 'docker', # Desktop client
		# 'devpod',
		
		# The below are paid apps and will require license keys after the trial.
		# 'bartender',
		# 'cleanshot',
		# 'downie',
		# 'homerow', # Students can email the developer for a discount.
		# 'middle',
		# 'multitouch',
		# 'shottr',
	]
	
	if include_copyq:
		# CopyQ requires Accessibility access to the system,
		# 	and requires that access to be reapproved after each update.
		# 	See the Homebrew Caveat section for instructions.
		packages.append('copyq')
	
	if include_arduino:
		packages.append('arduino-ide')
	
	# These apps might be of some interest, but do not currently have Casks:
	# 	Charmstone,
	# 	Keyboard Maestro,
	# 	rcmd,
	# 	Sleeve 2,
	# 	Umbra,
	
	if extras is not None:
		packages = extras
	
	brew_cmd = _DISTRO_FAMILY.install_steps[0]
	base_cmd = [*brew_cmd, 'install', '--quiet', '--cask']
	
	log(f"\t     run: {' '.join(base_cmd)} {' '.join(packages)}")
	if not _RUNNING_DRY:
		_ = run([*base_cmd, *packages], stdout=stdout, check=True, encoding='utf-8')
	
	git_cmd = ['git', 'config', 'remote.origin.url']
	remote_url = run(git_cmd, capture_output=True, check=True, encoding='utf-8').stdout.strip()
	plug = '/'.join([part for part in split(r'[\.:/]', remote_url) if part != 'git'][-2:])
	repo_url = f"https://gitlab.com/{plug}.git"
	
	log(f"\t     tap: {repo_url}")
	if not _RUNNING_DRY:
		_ = run([*brew_cmd, 'tap', plug, repo_url], stdout=stdout, check=True, encoding='utf-8')
	
	local_casks = (get_env_dir() / 'Casks').glob('*.rb')
	for cask in local_casks:
		log(f"\t     run: {' '.join(base_cmd)} {cask.stem}")
		if not _RUNNING_DRY:
			_ = run([*base_cmd, cask.stem], stdout=stdout, check=True, encoding='utf-8')

def install_os_packages(extras: list[str] | None = None, **kwargs) -> None:
	'''Install preferred system packages.'''
	
	# pylint: disable=too-many-branches(R0912),too-many-locals(R0914),consider-using-with(R1732)
	
	from stat import S_IRUSR, S_IWUSR, S_IRGRP
	
	doas_conf = Path('/etc/doas.conf')
	
	if _DISTRO_FAMILY is DistroFamily.REDHAT:
		configure_dnf()
		
		if doas_conf.is_file():
			log(f"\t  unlink: {doas_conf}")
			if not _RUNNING_DRY:
				doas_conf.unlink()
	
	elif _DISTRO_FAMILY is DistroFamily.ALPINE:
		log('\t  enable: apk community repo')
		if not _RUNNING_DRY:
			sed_cmd = ['sed', '-ri', r'/edge/! s%^#(.*/community)$%\1%']
			_ = run([*sed_cmd, '/etc/apk/repositories'], stdout=stdout, check=True)
	
	# macOS system management binaries are built another way and signed, so this isn't going to work.
	if _DISTRO_FAMILY is not DistroFamily.MACOS and not doas_conf.is_file():
		log(f"\t   write: {doas_conf}")
		if not _RUNNING_DRY:
			with doas_conf.open('w', encoding='utf-8') as conf:
				conf.write('permit persist :wheel as root\n')
				conf.write('permit persist :sudo  as root\n')
			doas_conf.chmod(S_IRUSR | S_IWUSR | S_IRGRP)
	
	packages = get_packages_for_system(**kwargs)
	
	if _DISTRO_NAME == 'ubuntu':
		log('\t disable: apt news feed')
		if not _RUNNING_DRY:
			ua_cmd = ['ubuntu-advantage', 'config', 'set', 'apt_news=false']
			_ = run(ua_cmd, stdout=stdout, check=True)
	
	if extras is not None and len(extras) != 0:
		packages = extras
		
		add_ppa('git-core/ppa')
		
		if _DISTRO_NAME == 'ubuntu': # noqa: SIM102
			if kwargs.get('include_copyq', False):
				add_ppa('hluk/copyq')
				for cq_pkg in ('copyq-doc', 'copyq-plugins'):
					if cq_pkg in packages:
						packages.remove(cq_pkg)
	
	else:
		packages.extend(get_gui_packages())
		if _DISTRO_FAMILY is not DistroFamily.MACOS and _RUNNING_GUI and 'vim' in packages:
			packages.remove('vim')
	
	(base_cmd, steps, distro_input) = _DISTRO_FAMILY.install_steps
	steps[-1] += f" {' '.join(packages)}"
	
	for step in steps:
		log(f"\t     run: {' '.join(base_cmd)} {step}")
		if not _RUNNING_DRY:
			this_step = base_cmd + step.split()
			_ = run(this_step, input=distro_input, stdout=stdout, check=True, encoding='utf-8')
	
	if _DISTRO_FAMILY is DistroFamily.MACOS:
		install_casks(**kwargs)

def upgrade_os_packages() -> None:
	'''Upgrade system packages.'''
	
	(base_cmd, steps) = _DISTRO_FAMILY.upgrade_steps
	for step in steps:
		log(f"\t     run: {' '.join(base_cmd)} {step}")
		if not _RUNNING_DRY:
			_ = run(base_cmd + ([step] if step != '' else []), stdout=stdout, check=True)

def generate_locale(locale: str = _PREFERED_LOCALE, /) -> None:
	'''Generate locale files for the given locale.'''
	
	# log('\t   check: available locales')
	locale_name = locale.replace('-', '').replace('UTF', 'utf')
	locale_list = run(['locale', '-a'], capture_output=True, check=True, encoding='utf-8')
	
	if locale_name not in locale_list.stdout:
		log(f"\t     run: locale-gen {locale}")
		if not _RUNNING_DRY:
			_ = run(['locale-gen', locale], stdout=stdout, check=True)
			_ = run(['update-locale'], stdout=stdout, check=True)

def copy_matching_git_contrib() -> None:
	'''Copy the files from git/<version>/contrib/completion to the local system.'''
	
	if _DISTRO_FAMILY is DistroFamily.MACOS:
		# log('\t    skip: copy git contrib (not needed)', stderr)
		return
	
	if which('git') is None:
		log('\t  refuse: git is not installed.', stderr)
		return
	
	output = run(['git', '--version'], capture_output=True, check=True, encoding='utf-8')
	git_version = output.stdout.strip().split()[2]
	
	base_url = f"https://raw.githubusercontent.com/git/git/v{git_version}/contrib/completion"
	system_location = Path('/usr/share/doc/git/contrib/completion')
	
	if not system_location.is_dir():
		log(f"\t makedir: {system_location}")
		if not _RUNNING_DRY:
			system_location.mkdir(parents=True, exist_ok=True)
	
	for script_file in ('git-completion.bash', 'git-completion.zsh', 'git-prompt.sh'):
		script_dest = system_location / script_file
		if not script_dest.exists():
			log(f"\t   fetch: {script_file} for v{git_version}")
			if not _RUNNING_DRY:
				try:
					incoming_file = get(f"{base_url}/{script_file}", timeout=3, stream=True)
					incoming_file.raise_for_status()
				except RequestException:
					log(f"\t    fail: Could not get {script_file} for v{git_version}", stderr)
				else:
					if incoming_file.ok:
						with script_dest.open('w', encoding='utf-8') as script:
							for line in incoming_file.iter_lines(decode_unicode=True):
								script.write(line + '\n')

def install_vscode() -> None:
	'''Install VSCode and supporting libraries.'''
	
	# pylint: disable=line-too-long(C0301)
	
	if not _RUNNING_DRY:
		if _DISTRO_NAME == 'raspbian':
			install_os_packages(['code'])
			return
		
		if _DISTRO_FAMILY is DistroFamily.MACOS:
			install_casks(['visual-studio-code'])
			return
	
	(file_type, manager) = _DISTRO_FAMILY.pkg_management
	
	if file_type in ('', 'apk', 'xbps'):
		log(f"\t  refuse: install VSCode; incompatible distro type ({_DISTRO_FAMILY})", stderr)
		return
	
	# ~ The package version is preferred to the tarball.
	
	machine_text = {
		'armv7': 'armhf',
		'aarch64': 'arm64',
		'x86_64': 'x64',
	}.get(_HOST_MACHINE, 'DUMMY')
	
	base_url = f"https://code.visualstudio.com/sha/download?build=stable&os=linux-{file_type}-{machine_text}"
	
	log(f"\t   fetch: vscode.{file_type}")
	if not _RUNNING_DRY:
		tmp_file = Path(f"/tmp/code.{file_type}")
		try:
			incoming_file = get(base_url, timeout=3, stream=True)
			incoming_file.raise_for_status()
		except RequestException:
			log(f"\t    fail: Could not get {base_url}", stderr)
			return
		
		if incoming_file.ok:
			with tmp_file.open('wb') as code_repo:
				for chunk in incoming_file.iter_content(chunk_size=256):
					code_repo.write(chunk)
		
		_ = run([manager, '-y', 'install', str(tmp_file)], stdout=stdout, check=True)
		
		install_os_packages(['code'])

def install_neovim() -> None:
	'''Install NeoVim and supporting libraries.'''
	
	# pylint: disable=no-else-return(R1705)
	
	from tempfile import NamedTemporaryFile
	
	# Currently Neovim does not provide arm* packages bcs GitHub does not have arm* runners yet.
	# 	Remove the below exception for Debian:!x86_64 when that package is available
	
	if _DISTRO_FAMILY in (
		DistroFamily.MACOS,
		DistroFamily.ALPINE,
		DistroFamily.REDHAT,
	) or (_DISTRO_FAMILY is DistroFamily.DEBIAN and not _X64_HOST):
		if not _RUNNING_DRY:
			install_os_packages(['neovim'])
		return
	
	nvim_url = 'https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz'
	log('\t   fetch: nvim-linux64')
	if not _RUNNING_DRY:
		try:
			incoming_file = get(nvim_url, timeout=3, stream=True)
			incoming_file.raise_for_status()
		except RequestException:
			log('\t    fail: Could not get nvim-linux64', stderr)
			return
		else:
			if incoming_file.ok:
				# pylint: disable=consider-using-with(R1732)
				tmp_file = NamedTemporaryFile(delete=False) # noqa: SIM115
				
				with tmp_file:
					for block in incoming_file.iter_content(chunk_size=128):
						tmp_file.write(block)
				
				_ = run([
					'tar',
					'--strip-components=1',
					'--no-overwrite-dir',
					'-C', '/usr',
					'-xzhf',
					tmp_file.name,
				], check=True)
				
				Path(tmp_file.name).unlink()

def configure_flatpak() -> None:
	'''Ensure that flathub is registered as a remote.'''
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS:
		log('\t  enable: flatpak remote (flathub)')
		if not _RUNNING_DRY:
			remote_cmd = [
				'flatpak',
				'--system',
				'remote-add',
				'--if-not-exists',
				'flathub',
				'https://dl.flathub.org/repo/flathub.flatpakrepo',
			]
			_ = run(remote_cmd, stdout=stdout, check=True)

def ensure_flatpaks() -> None:
	'''Make sure that system utility flatpaks are installed and up-to-date.'''
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS and _RUNNING_GUI:
		flatpaks = [
			'com.mattjakeman.ExtensionManager',
		]
		
		for pak in flatpaks:
			log(f"\t flatpak: install/update {pak}")
			if not _RUNNING_DRY:
				_ = run([
					'flatpak', 'install', '--or-update', '--system', '--noninteractive', 'flathub', pak,
				], stdout=stdout, check=True) # yapf: disable

def enable_linger(user: str | None = None, /) -> None:
	'''Enable process lingering for this user account.'''
	
	if which('loginctl') is not None and user is not None:
		check_cmd = ['loginctl', 'show-user', user, '--property=Linger']
		linger_status = run(check_cmd, capture_output=True, check=True, encoding='utf-8')
		if linger_status.stdout.strip().endswith('yes'):
			return
		
		linger_cmd = ['loginctl', 'enable-linger', user]
		log(f"\t     run: {' '.join(linger_cmd)}")
		if not _RUNNING_DRY:
			_ = run(linger_cmd, stdout=stdout, check=True)

def make_parser() -> ArgumentParser:
	'''Make and return a suitable cmd line argument parser.'''
	
	from argparse import RawTextHelpFormatter as RTHF
	
	# Make changes to line 3 to change the header of the parser.
	epilog = 'Shell completion for this script is generated on first use of the environment.'
	parser = ArgumentParser(
		description=globals()['__doc__'], epilog=epilog, formatter_class=RTHF, allow_abbrev=False,
	)
	
	quiet_help = 'suppress trace messages'
	parser.add_argument('-q', '--quiet', action='store_true', help=quiet_help, dest='quiet_run')
	
	very_quiet_help = 'suppress all output'
	parser.add_argument('-Q', '--silent', action='store_true', help=very_quiet_help, dest='silent_run')
	
	dry_help = 'dry run; take no action(s)'
	parser.add_argument('-d', '--dry', action='store_true', help=dry_help, dest='dry_run')
	
	linger_help = 'enable process lingering for given user (SystemD only) (default: SUPER_USER)'
	parser.add_argument('--linger', nargs='?', help=linger_help, metavar='USER', dest='linger_user')
	
	locale_help = f"generate locale; default: '{_PREFERED_LOCALE}'"
	parser.add_argument(
		'--locale',
		nargs='?',
		const=_PREFERED_LOCALE,
		help=locale_help,
		metavar='LOCALE',
		dest='verify_locale',
	)
	
	arduino_help = 'install/update arduino cross-compiler'
	parser.add_argument('--arduino', action='store_true', help=arduino_help, dest='include_arduino')
	
	code_help = 'install/update vscode'
	parser.add_argument('--vscode', action='store_true', help=code_help, dest='install_code')
	
	nvim_help = 'install/update neovim'
	parser.add_argument('--nvim', action='store_true', help=nvim_help, dest='install_nvim')
	
	copyq_help = 'install/update copyq'
	parser.add_argument('--copyq', action='store_true', help=copyq_help, dest='include_copyq')
	
	return parser

def main(arguments: Sequence[str], /) -> int:
	'''Run sequence of steps to setup this host with root permissions.'''
	
	# pylint: disable=global-statement(W0603)
	
	from platform import system
	
	if _DISTRO_FAMILY is not DistroFamily.UNKNOWN:
		# pylint: disable=no-name-in-module(E0611)
		from os import geteuid # type: ignore[attr-defined]
	
	global stdout # noqa: PLW0603
	global _RUNNING_DRY # noqa: PLW0603
	global _RUNNING_QUIET # noqa: PLW0603
	
	parser = make_parser()
	args = parser.parse_args(arguments)
	
	if system() == 'Windows':
		return 2
	
	_RUNNING_DRY = args.dry_run
	_RUNNING_QUIET = args.quiet_run
	
	if args.silent_run:
		# pylint: disable=consider-using-with(R1732)
		_RUNNING_QUIET = True
		stdout = Path(devnull).open('a', encoding='utf-8') # noqa: SIM115
	
	if _DISTRO_NAME == 'ubuntu':
		get_ppa_list()
	
	if args.verify_locale is None and _DISTRO_FAMILY is not DistroFamily.MACOS:
		args.verify_locale = _PREFERED_LOCALE
		# if get_distro_family() is DistroFamily.ALPINE:
		# 	args.verify_locale = args.verify_locale.split('.')[0]
	
	# print(args)
	# sys_exit(0)
	
	# Users need to be a member of group `vboxsf` on the guest to access a shared folder.
	
	super_user = environ.get('SUDO_USER') or environ.get('DOAS_USER') or ''
	
	# pylint: disable=possibly-used-before-assignment(E0606)
	if not _RUNNING_DRY and (super_user == '' or geteuid() != 0):
		log('\t  refuse: Re-run this script with sudo or doas.')
		return 1
	
	enable_linger(super_user)
	
	if args.verify_locale is not None and \
			_DISTRO_FAMILY not in (DistroFamily.MACOS, DistroFamily.ALPINE):
		generate_locale(args.verify_locale)
	
	install_os_packages(
		include_arduino=args.include_arduino,
		include_copyq=args.include_copyq,
	)
	
	if args.install_code:
		install_vscode()
	
	if args.install_nvim:
		install_neovim()
	
	configure_flatpak()
	ensure_flatpaks()
	upgrade_os_packages()
	enable_services()
	copy_matching_git_contrib()
	
	# Clean up after self; files could be owned by root.
	if (pycache := get_env_dir() / '__pycache__').is_dir():
		rmtree(pycache)
	
	return 0

if __name__ == '__main__':
	sys_exit(main(argv[1:]))

# vim: set ts=4 sw=0 sts=-1 noet ai sta:

#!/bin/zsh

# See: https://zsh.sourceforge.io/Doc/Release/zsh_toc.html for current docs.
# See: https://grml.org/zsh/zsh-lovers.html for misc examples.

unsetopt BEEP

setopt AUTO_CD
setopt AUTO_NAME_DIRS
setopt HIST_IGNORE_SPACE
setopt HIST_NO_STORE
setopt INC_APPEND_HISTORY
setopt INTERACTIVE_COMMENTS
setopt LIST_ROWS_FIRST
setopt PROMPT_SUBST
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_SILENT

umask 0077

mesg n || true
ttyctl -f
if [[ -z "${SSH_CONNECTION:-}" ]]; then
	xhost - &>/dev/null # enable access control for remote sessions
fi

if [[ 'alacritty' == "${TERM}" || 'alacritty' == "${TERM_PROGRAM:-}" ]]; then
	alias edit_alacritty='${EDITOR} ${XDG_CONFIG_HOME}/alacritty/alacritty.toml'
	
elif [[ 256 -gt "$(tput colors)" ]]; then
	typeset -gx TERM='xterm-256color'
fi

NEWLINE=$'\n'
typeset -g KEYTIMEOUT=5
typeset -g HISTFILE="${XDG_STATE_HOME}/zsh/history"
typeset -g HISTSIZE=999999999
typeset -g SAVEHIST="${HISTSIZE}"

typeset -gx GROFF_NO_SGR=1
typeset -gx MANPAGER='less -+X -iRx4'
typeset -gx PAGER="${MANPAGER}"
typeset -gx EDITOR='vim'
typeset -gx GPG_TTY="$(tty)"
typeset -gx SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
unset SSH_AGENT_PID

typeset -g expand_aliases=1
typeset -g expand_alias_recursive=1

if [[ -z "${HOMEBREW_PREFIX:-}" ]]; then
	eval "$(dircolors -b)"
fi

# `zkbd` can be used to determine what codes
# 	the (OS + keyboard + terminal_emulator + settings)
# 	sends for the extra keys on this system.
# `^V` can also be used to inspect a single key chord.
autoload -Uz zkbd

# Home
bindkey "\E[1~" beginning-of-line
bindkey "\E[7~" beginning-of-line
bindkey "\E[H" beginning-of-line
bindkey "\EOH" beginning-of-line

# End
bindkey "\E[4~" end-of-line
bindkey "\E[8~" end-of-line
bindkey "\E[F" end-of-line
bindkey "\EOF" end-of-line

# Ctrl+Left
bindkey "\EOD" backward-word
bindkey "\E[5D" backward-word
bindkey "\E[1;5D" backward-word
bindkey "\Eb" backward-word # Option+Left

# Ctrl+Right
bindkey "\EOC" forward-word
bindkey "\E[5C" forward-word
bindkey "\E[1;5C" forward-word
bindkey "\Ef" forward-word # Option+Right

bindkey "\E[2~" quoted-insert # Insert
bindkey "\E[3~" delete-char # Delete
bindkey "\E[5~" history-search-backward # Page-Up
bindkey "\E[6~" history-search-forward # Page-Down
bindkey "\EOM" accept-line
# bindkey -s "\E[24~" ''
# bindkey -s "\CL" "clear\n"
bindkey "\CR" history-incremental-pattern-search-backward

function prevd() {
	pushd -0
}
zle -N prevd
bindkey "\E[1;3D" prevd # Alt+Left

function nextd() {
	pushd +1 2>/dev/null
}
zle -N nextd
bindkey "\E[1;3C" nextd # Alt+Right

typeset -Ua _loaded_extensions
_loaded_extensions=()

typeset -Ua _ignore_functions
_ignore_functions=('_.*')
_ignore_functions+=('.*zle.*')
_ignore_functions+=('is-at-least')
_ignore_functions+=('getent') # macOS provides a wrapper for this.

# Up-Arrow
autoload -Uz up-line-or-beginning-search
_ignore_functions+=('up-line-or-beginning-search')
zle -N up-line-or-beginning-search
bindkey "\EOA" up-line-or-beginning-search
bindkey "\E[A" up-line-or-beginning-search

# Down-Arrow
autoload -Uz down-line-or-beginning-search
_ignore_functions+=('down-line-or-beginning-search')
zle -N down-line-or-beginning-search
bindkey "\EOB" down-line-or-beginning-search
bindkey "\E[B" down-line-or-beginning-search

git_completion_path='/usr/share/doc/git/contrib/completion'
if [[ ! -d "${git_completion_path}" ]]; then
	if [[ -d '/usr/share/git/completion' ]]; then
		git_completion_path='/usr/share/git/completion'
	elif [[ -n "${HOMEBREW_PREFIX:-}" ]]; then
		git_completion_path="${HOMEBREW_PREFIX}/etc/bash_completion.d"
	fi
fi

# Do NOT include a ';' at the end of an alias!
alias eenv="\${EDITOR} ${HOME}/.zshenv"
alias ez="\${EDITOR} ${HOME}/.zshrc"
alias ezp="\${EDITOR} ${HOME}/.zsh_personal"
alias senv="source ${HOME}/.zshenv"
alias sz="senv; source ${HOME}/.zshrc"
alias ls="ls --color=auto -Fh${HOMEBREW_PREFIX:+G}"
alias ll='ls -l'
alias la='ll -av'
alias l.='ll -d .*'
alias l,='ll -dr .. .'
alias less="${MANPAGER}"
alias ssh='ssh -X -o LogLevel=ERROR'

hash -d "$(whoami)=${HOME:A}"
hash -d "$(basename ${env_repo})=${env_repo}"

function load_extension() {
	if [[ 1 -ne ${#} ]]; then
		echo "usage: ${funcstack[1]} EXTENSION_NAME" >&2
		echo "You need to provide an extension to load." >&2
		return 1
	fi
	
	local extension="${env_repo}/extensions/${1}"
	source "${extension}" && _loaded_extensions+=("${1}") || (echo "'${extension}' is not available." && true)
}

typeset -a extensions
extensions=()
extensions+=('functions')
extensions+=('aliases')
extensions+=('completion')
extensions+=('styles')
extensions+=('expand_shortcuts')
extensions+=('tool_themes')
extensions+=('prompt')

for fragment in "${extensions[@]}"; do
	load_extension "${fragment}"
done

typeset -gx env_man_dir="${XDG_DATA_HOME}/man"
# This needs to end in a ':' so the normal search path will be appended, instead of replaced.
typeset -Ugx MANPATH="${env_man_dir}${HOMEBREW_PREFIX:+:${HOMEBREW_PREFIX}/share/man}:"

if [[ -r "${HOME}/.zsh_personal" ]]; then
	source "${HOME}/.zsh_personal"
fi

function env_update() {
	local has_changes=0
	
	pushd -q "${env_repo}"
	git fetch --all --prune
	
	if [[ 1 -lt "$(git status --ignored --short | wc -l)" ]]; then
		has_changes=1
		git stash push --all
	fi
	git pull --stat
	if [[ 0 -ne "${has_changes}" ]]; then
		git stash pop
	fi
	
	popd -q
}

if [[ -n "$(whence direnv)" && -n "${ENABLE_DIRENV:=}" ]]; then
	eval "$(direnv hook zsh)"
fi

# This is needed bcs something in the default startup scripts puts zcompdump in ZDOTDIR:-HOME.
if [[ -e "${ZDOTDIR:-${HOME}}/.zcompdump" ]]; then
	rm "${ZDOTDIR:-${HOME}}/.zcompdump"
fi
: ${COMPDUMP_LOC:=${XDG_CACHE_HOME}/zsh/zcompdump}
autoload -Uz compinit && compinit -d "${COMPDUMP_LOC}"
autoload -Uz bashcompinit && bashcompinit
_ignore_functions+=('(?:bash)?comp.*')

function rebuild_completions() {
	rm -f "${ZDOTDIR:-${HOME}}/.zcompdump" "${COMPDUMP_LOC}"
	compinit -d "${COMPDUMP_LOC}"
}

# This MUST be loaded after ALL zle, extensions, and *compinit commands.
for fragment in $(find ${env_repos_dir} -type f -name 'zsh-syntax-highlighting.zsh'); do
	source "${fragment}"
done

unset manpage manpages section fragment

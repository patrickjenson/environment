#!/bin/zsh

unsetopt GLOBAL_RCS

unset GCC_EXEC_PREFIX
unset GIT_EXEC_PATH
unset LD_LIBRARY_PATH
unset LD_PRELOAD

# See: '/etc/zshrc_Apple_Terminal' for details.
typeset -gx SHELL_SESSIONS_DISABLE=1
if [[ -d "${HOME}/.zsh_sessions/" ]]; then
	rm -rf "${HOME}/.zsh_sessions/"
fi

# Shortcut to modify path; if the 2nd argument is the word 'after',
# 	then the new item is appended instead of prepended.
function modify_path() {
	if [[ -z "${1:-}" || -n "$($(whence -p grep) ':' <<< ${1})" || 3 -le ${#} ]]; then
		echo "You must provide exactly one value to be added to path." >&2
		return 1
	fi
	
	if [[ ! -d "${1}" ]]; then
		echo "\"${1}\" is not a directory; Ignoring..." >&2
		return
	elif [[ ! -x "${1}" ]]; then
		echo "You don't have execute permission to \"${1}\"; Ignoring..." >&2
		return
	fi
	
	if [[ 'after' == "${2:-}" ]]; then
		path+=("${1}")
	else
		path=("${1}" ${path})
	fi
}

typeset -Ugx PATH path
path=()
modify_path '/sbin'
modify_path '/usr/sbin'
modify_path '/usr/local/sbin' 2>/dev/null
modify_path '/bin'
modify_path '/usr/bin'
modify_path '/usr/local/bin' 2>/dev/null
modify_path '/usr/lib/cargo/bin' 2>/dev/null

if [[ 0 -ne "${USE_SNAPS:-1}" && -n "$(whence snap)" ]]; then
	modify_path '/snap/bin' 2>/dev/null
fi

# XDG_RUNTIME_DIR is normally ~= '/run/user/${UID}', but should be set by the system.
typeset -gx XDG_SESSION_TYPE="${XDG_SESSION_TYPE:-tty}"
typeset -gx XDG_CACHE_HOME="${XDG_CACHE_HOME:-${HOME}/.cache}"
typeset -gx XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-${HOME}/.config}"
typeset -gx XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
typeset -gx XDG_STATE_HOME="${XDG_STATE_HOME:-${HOME}/.local/state}"
typeset -gx XDG_BIN_HOME="${XDG_BIN_HOME:-${HOME}/.local/bin}" # This is not exactly canon, but fits the pattern.
for dpath in $(env | sed -rn 's%^XDG_.*_HOME=(.*)$%\1%p'); do
	if [[ ! -d "${dpath}/zsh" ]]; then
		mkdir -p "${dpath}/zsh"
	fi
done

if [[ 0 -ne "${USE_FLATPAK:-1}" && -n "$(whence flatpak)" ]]; then
	modify_path "${XDG_BIN_HOME}/flatpak" 2>/dev/null
	
	flatpak_suffix='/flatpak/exports/share'
	system_flatpaks="/var/lib${flatpak_suffix}"
	local_flatpaks="${XDG_DATA_HOME}${flatpak_suffix}"
	
	if [[ 0 -ne "${USE_RAW_FLATPAKS:-0}" ]]; then
		modify_path "${system_flatpaks}" 2>/dev/null
		modify_path "${local_flatpaks}" 2>/dev/null
	fi
	
	# Some distros do this step automatically.
	if [[ ! "${XDG_DATA_DIRS:-}" =~ 'flatpak' ]]; then
		typeset -gx XDG_DATA_DIRS="${XDG_DATA_DIRS:-/usr/local/share:/usr/share}"
		XDG_DATA_DIRS="${local_flatpaks}:${system_flatpaks}:${XDG_DATA_DIRS}"
	fi
	
	unset flatpak_suffix {system,local}_flatpaks
fi

typeset -gx GNUPGHOME="${XDG_DATA_HOME}/gnupg"
typeset -gx RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
typeset -gx CARGO_HOME="${XDG_DATA_HOME}/cargo"

typeset -gx MINIKUBE_HOME="${XDG_CONFIG_HOME}/.minikube" # final section MUST be '.minikube'
typeset -gx MINIKUBE_IN_STYLE=false
typeset -gx GHCUP_USE_XDG_DIRS=1
typeset -gx USE_EMOJI=0

hush_file="${HOME}/.hushlogin"
touch "${hush_file}"

if [[ 'Darwin' == "$(uname -s)" ]]; then
	typeset -gx XDG_SESSION_TYPE='macos'
	typeset -gx CLICOLOR=1
	
	if [[ 'Apple_Terminal' == "${TERM_PROGRAM:-}" ]]; then
		typeset -g colors_8bit=1
	fi
	
	typeset -gx HOMEBREW_NO_ANALYTICS=1
	typeset -gx HOMEBREW_NO_EMOJI=1
	typeset -gx HOMEBREW_BAT=1
	
	typeset -gx HOMEBREW_PREFIX='/opt/homebrew'
	modify_path "${HOMEBREW_PREFIX}/sbin"
	modify_path "${HOMEBREW_PREFIX}/bin"
	
	if [[ -r '/Applications/Alacritty.app' && ! -e "${HOME}/.terminfo/61/alacritty" ]]; then
		mkdir -p "${HOME}/.terminfo/61"
		ln -s '/Applications/Alacritty.app/Contents/Resources/61/alacritty' "${HOME}/.terminfo/61/alacritty"
	fi
else
	# The version that comes with macOS does not support this option.
	# The version that comes with Homebrew does support this option, but
	# 	it can't be used bcs compaudit complains that site-functions is not owned by (USER | root).
	setopt REMATCH_PCRE
	
	if [[ -f '/etc/motd' ]]; then # Do not check for links here.
		# See if the motd has changed since last login; *beware* timestamps.
		cmp -s '/etc/motd' "${hush_file}"
		
		if [[ 0 -ne ${?} ]]; then
			# Show the motd if it HAS changed and update the cache for next time.
			tee "${hush_file}" < '/etc/motd'
		fi
	fi
	
	if [[ 'tty' == "${XDG_SESSION_TYPE}" ]]; then
		typeset -g colors_8bit=1
	fi
fi

dpath='/usr/lib/jvm'
if [[ -d "${dpath}" ]]; then
	dpath=$(find "${dpath}" -mindepth 2 -maxdepth 2 -type d -name bin)
	dpath=$(${HOMEBREW_PREFIX:+g}sed -rn '/java-(1\.)?8/p' <<< "${dpath}")
	modify_path "${dpath}" 2>/dev/null
fi

this_file="$(readlink -f $(sed -r 's%^-?(.*)$%\1%' <<< ${(%):-%x}))"
typeset -gx env_repo="$(git -C ${this_file:h} rev-parse --show-toplevel)"
typeset -gx env_repos_dir="${HOME}/environment_repos"
typeset -gx env_completion_dir="${HOME}/.zsh"
typeset -Ugx CLASSPATH=".:${HOME}/bin:${XDG_BIN_HOME}"
typeset -gx repos_dir="${HOME}/repos"
typeset -gx venvs_dir="${HOME}/venvs"

modify_path "${env_repo}/scripts"
modify_path "${HOME}/bin"
modify_path "${XDG_BIN_HOME}"
modify_path "${CARGO_HOME}/bin" 2>/dev/null
modify_path "${venvs_dir}/global/bin" 2>/dev/null
modify_path '.'

# Some programs can crash if these aren't set properly.
locale_pref=en_US.UTF-8
typeset -gx LC_ALL="${locale_pref}"
typeset -gx LANG="${locale_pref}"
typeset -gx LANGUAGE="${locale_pref}"

unset dpath hush_file this_file locale_pref

'''Configuration file for ptpython.'''

# Should be located in:
# 	%LOCALAPPDATA%\prompt_toolkit\ptpython\config.py, or
# 	$HOME/Library/Preferences/ptpython/config.py, or
# 	${XDG_CONFIG_HOME:-$HOME/.config}/ptpython/config.py

# pylint: disable=missing-module-docstring(C0114),import-error(E0401)

from __future__ import annotations
from platform import system
from typing import TYPE_CHECKING

from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.keys import Keys
from prompt_toolkit.output import ColorDepth
from ptpython.completer import CompletePrivateAttributes # pylint: disable=no-name-in-module(E0611)
from ptpython.layout import CompletionVisualisation # pylint: disable=no-name-in-module(E0611)

if TYPE_CHECKING:
	from prompt_toolkit.key_binding.key_processor import KeyPressEvent
	from ptpython.repl import PythonRepl

__all__ = ['configure']

# This function must be called 'configure'.
def configure(repl: PythonRepl, /) -> None:
	'''Configure the ptpython session.'''
	
	# See: ptpython/python_input.py for current defaults.
	
	# See: get_ptpython() in running session to affect settings.
	# Pressing 'v' in NAV mode will open the input in the current editor.
	# Pressing meta-! will display the system prompt.
	
	repl.use_code_colorscheme('one-dark')
	
	# see: ptpython/entry_points/run_ptpython.py => run() for
	# 	starting point on disabling history from cmdline. PR?
	history = InMemoryHistory()
	repl.history = history
	repl.default_buffer.history = history
	
	repl.color_depth = ColorDepth.DEPTH_8_BIT
	repl.complete_private_attributes = CompletePrivateAttributes.IF_NO_PUBLIC
	repl.completion_visualisation = CompletionVisualisation.POP_UP
	repl.confirm_exit = False
	repl.cursor_shape_config = 'Blink block' # This is a magic constant.
	repl.enable_auto_suggest = True
	repl.enable_fuzzy_completion = True
	repl.highlight_matching_parenthesis = True
	repl.show_line_numbers = True
	repl.vi_mode = True
	
	# This does not clear correctly on Windows.
	# 	Seems to be a flaw in windll.kernel32.SetConsoleTitleW or its Python bindings.
	if system() == 'Windows':
		repl.terminal_title = None
	
	@repl.add_key_binding(Keys.F7)
	def _(_event: KeyPressEvent) -> None:
		repl.show_docstring = not repl.show_docstring
	
	# When enabled, highlighting with the mouse is disabled, and vice-versa.
	@repl.add_key_binding(Keys.F8)
	def _(_event: KeyPressEvent) -> None:
		repl.enable_mouse_support = not repl.enable_mouse_support

# vim: set ts=4 sw=0 sts=-1 noet ai sta:

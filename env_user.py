#!/usr/bin/env python3

'''A script to manage personal preferences for a host machine.'''

from argparse import ArgumentParser
from collections.abc import Sequence
from getpass import getuser
from os import devnull, environ, pathsep, umask
from pathlib import Path
from platform import machine, system
from shutil import which
from subprocess import run
from stat import S_ISGID, S_IRWXU, S_IRWXG, S_IRGRP, S_IXGRP, S_IRWXO, S_IROTH, S_IXOTH
from sys import argv, exit as sys_exit, stderr, stdout
from typing import TextIO

from requests import get, RequestException

from distro_util import DistroFamily

_HOST_SYSTEM = system()
_HOST_MACHINE = machine()
_DISTRO_FAMILY = DistroFamily.fetch()

_ = umask(S_IRWXG | S_IRWXO) # 0o0077

_RUNNING_DRY = False
_RUNNING_QUIET = False

def log(message: str, /, output_file: TextIO = stdout) -> None:
	'''Print a message to output_file if not _RUNNING_QUIET.'''
	
	if not _RUNNING_QUIET and output_file is not devnull:
		print(message, file=output_file)

_LATEST = 'latest'
_DIR_PERM_DEFAULT = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH # 0o0755

_HOME = Path.home()
_USER = getuser()

def ensure_env_path(variable: str, /, value: str | Path | None = None) -> Path:
	'''
	Ensure that an environment variable has a value, inserting the given value if needed.
	Returns a Path of the result.
	'''
	
	if value is not None and variable not in environ:
		if isinstance(value, str):
			value = Path(value)
		
		value = value.expanduser()
		
		environ[variable] = str(value)
		
		return value
	
	return Path(environ[variable]).expanduser()

USER_CONFIG_PATH = ensure_env_path('XDG_CONFIG_HOME', '~/.config')
USER_DATA_PATH = ensure_env_path('XDG_DATA_HOME', '~/.local/share')
USER_BINARY_PATH = ensure_env_path('XDG_BIN_HOME', '~/.local/bin')

PATH = environ['PATH'].split(pathsep)
if (bin_path := str(USER_BINARY_PATH)) not in PATH:
	PATH.insert(0, bin_path)
	environ['PATH'] = pathsep.join(PATH)

ENV_REPOS_DIR = ensure_env_path('env_repos_dir', _HOME / 'environment_repos')
ENV_COMP_DIR = ensure_env_path('env_completion_dir', _HOME / '.zsh')
ENV_MAN_DIR = ensure_env_path('env_man_dir', USER_DATA_PATH / 'man')
MAN1_DIR = ENV_MAN_DIR / 'man1'

def get_env_dir() -> Path:
	'''Get the location of the environment repo.'''
	
	from os.path import normpath
	
	path = Path(__file__)
	if path.is_symlink():
		path = path.readlink()
	return Path(normpath(str(path.parent)))

def enable_linger() -> None:
	'''Enable process lingering for this user account.'''
	
	if which('loginctl') is not None:
		check_cmd = ['loginctl', 'show-user', _USER, '--property=Linger']
		linger_status = run(check_cmd, capture_output=True, check=True, encoding='utf-8')
		if linger_status.stdout.strip().endswith('yes'):
			return
		
		linger_cmd = ['loginctl', 'enable-linger', _USER]
		log(f"\t     run: {' '.join(linger_cmd)}")
		log('\t    note: may require entering password')
		if not _RUNNING_DRY:
			_ = run(linger_cmd, stdout=stdout, check=True)

def correct_folder(folder: Path, /, mode: int = _DIR_PERM_DEFAULT) -> None:
	'''Ensure that a folder exists and has the correct permissions.'''
	
	if folder.is_dir():
		log(f"\t   chmod: {folder}")
		if not _RUNNING_DRY:
			folder.chmod(mode)
	
	else:
		log(f"\t makedir: {folder}")
		if not _RUNNING_DRY:
			folder.mkdir(mode, parents=True, exist_ok=True)

def adjust_mode(mode: int, /, keep_special_bits: bool = False) -> int:
	'''Adjust a permissions mode for systems that do not put a new user in their own group.'''
	
	# Linux puts new user accounts into a new group of the same name by default.
	# macOS puts new user accounts into group 'staff' by default.
	
	from stat import S_ISUID
	
	no_group = any([
		_DISTRO_FAMILY is DistroFamily.MACOS,
	])
	
	if no_group:
		mask: int = S_IRWXG # 0o0070
		
		if not keep_special_bits:
			mask |= S_ISUID | S_ISGID # 0o6000
		
		# Remove unwanted bits;
		mode &= ~(mask)
		
		# Duplicate Other bits into Group;
		mode |= (mode & S_IRWXO) << 3
	
	return mode

def correct_home_folder() -> None: # noqa: PLR0915
	'''Ensure that the permissions of home folder contents are correct.'''
	
	# pylint: disable=too-many-locals(R0914),too-many-statements(R0915)
	
	from itertools import chain
	from stat import S_IRUSR, S_IWUSR, S_IWGRP, S_IWOTH
	
	# ? What benefit would S_ISGID have here?
	home_mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IXOTH # 0o0751
	correct_folder(_HOME, adjust_mode(home_mode))
	
	def unwanted_file(name: str, /) -> bool:
		files_to_keep = [
			'.CFUserTextEncoding',
			'.DS_Store',
		]
		
		files_to_delete = [
			'.profile',
			'.lesshst',
			'.zcompdump',
			'.zhistory',
		]
		
		return name not in files_to_keep and any([
			name.startswith('.bash'),
			name.endswith('_history'),
			name in files_to_delete,
		])
	
	# environment
	default_directories = [
		'Desktop',
		'Documents',
		'Downloads',
		'Library', # macOS
		'Movies', # macOS
		'Music',
		'Pictures',
		# 'Public', # handled separately below.
		'Templates', # XDG
		'Videos', # XDG
	]
	
	for entry in (_HOME / entry for entry in _HOME.iterdir()):
		if entry.is_file() and unwanted_file(entry.name):
			log(f"\t  unlink: {entry}")
			if not _RUNNING_DRY:
				entry.unlink()
		
		elif entry.is_dir() and entry.name in default_directories:
			log(f"\t   chmod: {entry}")
			if not _RUNNING_DRY:
				entry.chmod(entry.stat().st_mode & ~(S_IRWXO))
	
	managed_public_dir = _HOME / 'Public'
	public_dir = _HOME / 'public'
	
	if not managed_public_dir.is_dir():
		correct_folder(public_dir)
	
	else:
		correct_folder(managed_public_dir)
		
		if not public_dir.exists():
			public_dir.symlink_to(managed_public_dir)
		
		elif public_dir.is_symlink():
			if not public_dir.samefile(managed_public_dir):
				log(f"\t warning: {public_dir} does not link to {managed_public_dir}")
		
		elif public_dir.is_dir():
			log(f"\t    skip: linking {public_dir} to {managed_public_dir}; (existing directory)")
		
		else:
			log(f"\t   error: linking {public_dir} to {managed_public_dir}; (file, or similar)")
	
	dropbox_mode = S_ISGID | S_IRWXU | S_IWGRP | S_IXGRP | S_IWOTH | S_IXOTH
	correct_folder(_HOME / 'dropbox', dropbox_mode) # 0o2733; No adjustment needed.
	
	# This needs to get updated if this script ever moves relative to the repo root.
	this_repo = Path(__file__).resolve().parent
	
	log(f"\t   chmod: {this_repo}")
	if not _RUNNING_DRY:
		mask = ~(S_IRWXG | S_IRWXO) # 0o0077; No adjustment needed.
		
		this_repo.chmod(this_repo.stat().st_mode & mask)
		for (dirpath, dirnames, filenames) in this_repo.walk(top_down=False):
			for env_item in (dirpath / item for item in chain(filenames, dirnames)):
				env_item.chmod(env_item.stat().st_mode & mask)
	
	correct_folder(USER_CONFIG_PATH, S_IRWXU)
	correct_folder(USER_DATA_PATH, S_IRWXU)
	correct_folder(USER_BINARY_PATH, S_IRWXU)
	
	correct_folder(_HOME / 'bin', S_IRWXU)
	correct_folder(ENV_REPOS_DIR, S_IRWXU)
	correct_folder(ENV_COMP_DIR, S_IRWXU)
	correct_folder(ENV_MAN_DIR, S_IRWXU)
	correct_folder(MAN1_DIR, S_IRWXU)
	
	correct_folder(ensure_env_path('repos_dir', _HOME / 'repos'), S_IRWXU)
	correct_folder(ensure_env_path('venvs_dir', _HOME / 'venvs'), S_IRWXU)
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS and (USER_CONFIG_PATH / 'user-dirs.dirs').exists():
		log('\t disable: xdg-user-dirs-update')
		if not _RUNNING_DRY:
			with (USER_CONFIG_PATH / 'user-dirs.conf').open('w') as conf:
				conf.write('enabled=False\n')
	
	sudo_success = _HOME / '.sudo_as_admin_successful'
	if sudo_success.exists():
		log(f"\t   chmod: {sudo_success}")
		if not _RUNNING_DRY:
			sudo_success.chmod(sudo_success.stat().st_mode & ~(S_IRGRP | S_IROTH))
	
	user_rw = S_IRUSR | S_IWUSR # 0o0600; No adjustment needed.
	
	gnu_dir = ensure_env_path('GNUPGHOME', USER_DATA_PATH / 'gnupg')
	correct_folder(gnu_dir, S_IRWXU)
	
	gpg_file = gnu_dir / 'gpg-agent.conf'
	if not gpg_file.is_file():
		log(f"\t   write: {gpg_file}")
		if not _RUNNING_DRY:
			with gpg_file.open('w', encoding='utf-8') as gpg_conf:
				gpg_conf.write('sh\nenable-ssh-support\n')
				
				if _DISTRO_FAMILY is DistroFamily.MACOS:
					gpg_conf.write('pinentry-program /opt/homebrew/bin/pinentry-mac\n')
			
			gpg_file.chmod(user_rw)
	
	ssh_dir = _HOME / '.ssh'
	correct_folder(ssh_dir, S_IRWXU)
	
	config_file = ssh_dir / 'config'
	log(f"\t   touch: {config_file}")
	if not _RUNNING_DRY:
		mode = user_rw
		config_file.touch(mode)
		config_file.chmod(mode)
	
	hosts = ssh_dir / 'known_hosts'
	log(f"\t   touch: {hosts}")
	if not _RUNNING_DRY:
		mode = user_rw
		hosts.touch(mode)
		hosts.chmod(mode)
	
	auth_keys = ssh_dir / 'authorized_keys'
	log(f"\t   touch: {auth_keys}")
	if not _RUNNING_DRY:
		mode = user_rw | S_IRGRP | S_IROTH # 0o0644; No adjustment needed.
		auth_keys.touch(mode)
		auth_keys.chmod(mode)

def publish_env() -> None:
	'''Link tracked files to the normal location.'''
	
	# pylint: disable=too-many-locals(R0914)
	
	from datetime import datetime
	
	files_to_link = [
		# '.gitconfig', # using $XDG_CONFIG_HOME/git/config instead
		# 'tmux.conf', # using $XDG_CONFIG_HOME/tmux/tmux.conf instead
		# '.vimrc', # using $HOME/.vim/vimrc instead
		'.zshenv',
		'.zshrc',
	]
	
	config_path = Path('config_files')
	file_locations = dict(
		zip(
			(_HOME / curr_file for curr_file in files_to_link),
			(config_path / curr_file for curr_file in files_to_link),
			strict=False,
		),
	)
	
	git_dir = USER_CONFIG_PATH / 'git'
	tmux_dir = USER_CONFIG_PATH / 'tmux'
	vim_dir = _HOME / '.vim'
	mypy_dir = USER_CONFIG_PATH / 'mypy'
	yapf_dir = USER_CONFIG_PATH / 'yapf'
	rustfmt_dir = USER_CONFIG_PATH / 'rustfmt'
	ptpython_dir = USER_CONFIG_PATH / 'ptpython'
	ruff_dir = USER_CONFIG_PATH / 'ruff'
	alacritty_dir = USER_CONFIG_PATH / 'alacritty'
	haskell_dir = USER_CONFIG_PATH / 'ghc'
	
	folders = [
		git_dir,
		tmux_dir,
		vim_dir,
		mypy_dir,
		yapf_dir,
		rustfmt_dir,
		ptpython_dir,
		ruff_dir,
		alacritty_dir,
		haskell_dir,
	]
	
	for folder in folders:
		if not folder.is_dir():
			log(f"\t makedir: {folder}")
			if not _RUNNING_DRY:
				folder.mkdir(parents=True, exist_ok=True)
	
	file_locations[mypy_dir / 'config'] = Path('mypy.ini')
	file_locations[USER_CONFIG_PATH / 'pylintrc'] = Path('.pylintrc')
	file_locations[yapf_dir / 'style'] = Path('.style.yapf')
	file_locations[ruff_dir / 'ruff.toml'] = Path('ruff.toml')
	file_locations[git_dir / 'config'] = config_path / '.gitconfig'
	file_locations[tmux_dir / 'tmux.conf'] = config_path / 'tmux.conf'
	file_locations[vim_dir / 'vimrc'] = config_path / '.vimrc'
	file_locations[rustfmt_dir / 'rustfmt.toml'] = config_path / 'rustfmt.toml'
	file_locations[ptpython_dir / 'config.py'] = config_path / 'ptpython.py'
	file_locations[alacritty_dir / 'alacritty.toml'] = config_path / 'alacritty.toml'
	file_locations[ENV_COMP_DIR / '_load_extension'] = config_path / '_load_extension'
	file_locations[haskell_dir / 'ghci.conf'] = config_path / 'ghci.conf'
	file_locations[_HOME / '.ghci'] = config_path / 'ghci.conf'
	
	if _DISTRO_FAMILY is DistroFamily.MACOS:
		macos_app_support_dir = Path('~/Library/Application Support').expanduser()
		correct_folder(macos_app_support_dir, S_IRWXU)
		
		ruff_dir2 = macos_app_support_dir / 'ruff'
		correct_folder(ruff_dir2, S_IRWXU)
		file_locations[ruff_dir2 / 'ruff.toml'] = Path('ruff.toml')
		
		rustfmt_dir2 = macos_app_support_dir / 'rustfmt'
		correct_folder(rustfmt_dir2, S_IRWXU)
		file_locations[rustfmt_dir2 / 'rustfmt.toml'] = config_path / 'rustfmt.toml'
		
		macos_prefs_dir = Path('~/Library/Preferences').expanduser()
		correct_folder(macos_prefs_dir, S_IRWXU)
		
		ptpython_dir2 = macos_prefs_dir / 'ptpython'
		correct_folder(ptpython_dir2, S_IRWXU)
		file_locations[ptpython_dir2 / 'config.py'] = config_path / 'ptpython.py'
	
	# Make these scripts available in PATH. Adding the repo root to PATH is not desirable.
	this_file = Path(Path(__file__).name)
	file_locations[_HOME / 'bin' / this_file] = this_file
	root_script = Path('env_root.py')
	file_locations[_HOME / 'bin' / root_script] = root_script
	venv_script = Path('venv_util.py')
	file_locations[_HOME / 'bin' / venv_script] = venv_script
	
	for (file_loc, env_file) in file_locations.items():
		link_dest = get_env_dir() / env_file
		if file_loc.exists():
			if file_loc.is_symlink():
				if file_loc.readlink() == link_dest:
					continue
				log(f"\t  unlink: {file_loc}")
				if not _RUNNING_DRY:
					file_loc.unlink()
			else:
				log(f"\t  rename: {file_loc}")
				if not _RUNNING_DRY:
					# ~Consider keeping track of the new names and printing a report at the end.
					now = datetime.now().strftime('%F_%R')
					file_loc.rename(f"{file_loc}.bkp_{now}")
		
		log(f"\t    link: {file_loc}")
		if not _RUNNING_DRY:
			file_loc.symlink_to(link_dest)
	
	if _DISTRO_FAMILY is DistroFamily.ALPINE:
		zlogout = _HOME / '.zlogout'
		log(f"\t   write: {zlogout}")
		if not _RUNNING_DRY:
			with zlogout.open('w', encoding='utf-8') as conf:
				conf.write('clear\n')
	
	# The bat binary gets created as 'batcat' on Debian/Ubuntu due to a naming conflict.
	if (batcat := which('batcat')) is not None and which('bat') is None:
		batcat_loc = Path(batcat).resolve()
		log(f"\t    link: bat -> {batcat_loc}")
		if not _RUNNING_DRY:
			(USER_BINARY_PATH / 'bat').symlink_to(batcat_loc)

def ensure_python_packages(extras: list[str] | None = None, /) -> None:
	'''Install needed python packages.'''
	
	from venv_util import CurrentInterpreterError, GlobalEnv
	
	extra_pkgs = extras or []
	
	log(f"\t  ensure: global venv with {extra_pkgs}")
	log(f"\t    note: default packages are [{', '.join(GlobalEnv.get_packages())}]")
	if not _RUNNING_DRY:
		try:
			GlobalEnv().build(extra_pkgs=extra_pkgs)
		
		except CurrentInterpreterError as e:
			log('\t  refuse: upgrade current interpreter')
			print(f"\t\t  {e.MESSAGE}\n\t\t  {e.hint(__file__, extra_pkgs)}")

def update_rustup() -> None:
	'''Install/update rustup to manage the rust framework.'''
	
	# pylint: disable=consider-using-with(R1732)
	
	# ~ Some cargo pkgs must use a build script bcs `cargo install` will not currently run a build.rs;
	# ~ 	see: https://github.com/rust-lang/cargo/issues/2729
	
	from subprocess import PIPE, Popen
	
	_ = ensure_env_path('RUSTUP_HOME', USER_DATA_PATH / 'rustup')
	rust_path = ensure_env_path('CARGO_HOME', USER_DATA_PATH / 'cargo') / 'bin'
	if (path := environ['PATH']).find('cargo') == -1:
		environ['PATH'] = f"{rust_path}:{path}"
	
	if not rust_path.is_dir():
		curl_cmd = ['curl', '-fsSL', 'https://sh.rustup.rs']
		shell = 'bash' # This script does not seem to be zsh compatible.
		log(f"\t     run: {' '.join(curl_cmd)} | {shell}")
		if not _RUNNING_DRY:
			curl_result = Popen(curl_cmd, stdout=PIPE)
			shell_cmd = [shell, '-s', '--', '-y', '--no-modify-path']
			_ = run(shell_cmd, stdin=curl_result.stdout, stdout=stdout, check=True)
			if curl_result.stdout is not None: # silence a mypy error
				curl_result.stdout.close() # forwards a SIGPIPE to the shell if needed.
	
	rustup = str(rust_path / 'rustup')
	for cmd in [('default', 'stable'), ('update', )]:
		log(f"\t     run: rustup {' '.join(cmd)}")
		if not _RUNNING_DRY:
			_ = run([rustup, *cmd], stdout=stdout, check=True)

def fetch_github_binary(
		repo: str,
		name: str,
		version: str = _LATEST,
		*,
		pattern: str = '{host}-{arch}',
	) -> None:
	'''
	Fetch the given version of a GitHub binary.
	
	The binary will be saved with its original name and linked to the requested `name`.
	
	`repo` should be like 'owner/product'.
	`name` is what the result should be linked as.
	`pattern` is how the correct binary should be chosen.
	'''
	
	# pylint: disable=too-many-locals(R0914)
	
	from stat import S_IXUSR
	
	if version != _LATEST:
		version = f"tags/v{version}"
	
	arch_table = {}
	
	arm32 = ['armv7', 'arm']
	arch_table.update(dict.fromkeys(arm32, arm32))
	
	arm64 = ['aarch64', 'arm64']
	arch_table.update(dict.fromkeys(arm64, arm64))
	
	amd64 = ['x86_64', 'x64', 'amd64']
	arch_table.update(dict.fromkeys(amd64, amd64))
	
	machine_options = arch_table.get(_HOST_MACHINE, 'DUMMY')
	host = _HOST_SYSTEM.lower()
	
	binary_url = None
	# ~ The below is not able to use a simple download URL bcs
	# ~ 	projects do not all name their binaries with a predicatable pattern.
	# ~ This already assumes that the requested binary will use a host suffix.
	asset_list = f"https://api.github.com/repos/{repo}/releases/{version}"
	try:
		for asset in get(asset_list, timeout=3).json()['assets']:
			item: str = asset['browser_download_url']
			
			for option in machine_options:
				if pattern.format(host=host, arch=option) in item:
					binary_url = item
					break
			
			if binary_url is not None:
				break
	
	except (RequestException, KeyError):
		log(f"\t   error: unable to fetch asset list for {name}", stderr)
		# no return here; want this message followed by next message and return
	
	if binary_url is None:
		log(f"\t   error: unable to find a valid binary for {name}", stderr)
		return
	
	binary_name = USER_BINARY_PATH / binary_url.split('/')[-1]
	
	log(f"\t   fetch: {binary_name} from {repo}")
	if not _RUNNING_DRY:
		if not binary_name.exists():
			try:
				incoming_file = get(binary_url, timeout=3, stream=True)
				incoming_file.raise_for_status()
				
			except RequestException:
				log(f"\t    fail: Could not get {binary_url}", stderr)
				return
			
			if incoming_file.ok:
				with binary_name.open('wb') as bin_file:
					for chunk in incoming_file.iter_content(chunk_size=256):
						bin_file.write(chunk)
			
			binary_name.chmod(binary_name.stat().st_mode | S_IXUSR)
		
		else:
			log(f"\t    note: {binary_name} is already installed")
	
	link_name = USER_BINARY_PATH / name
	
	log(f"\t    link: {link_name} to {binary_name}")
	if not _RUNNING_DRY and not link_name.exists():
		link_name.symlink_to(binary_name)

def install_direnv(version: str = _LATEST, /) -> None:
	'''Install the given version of direnv.'''
	
	fetch_github_binary('direnv/direnv', 'direnv', version)
	
	pages = [
		'direnv-fetchurl.1',
		'direnv-stdlib.1',
		'direnv.1',
		'direnv.toml.1',
	]
	for page in pages:
		page_loc = MAN1_DIR / page
		log(f"\t   fetch: {page}")
		if not _RUNNING_DRY and not page_loc.exists():
			man_url = 'https://raw.githubusercontent.com/direnv/direnv/master/man/'
			try:
				incoming_file = get(man_url + page, timeout=3, stream=True)
				incoming_file.raise_for_status()
			
			except RequestException:
				log(f"\t    fail: Could not get {page}", stderr)
			
			else:
				if incoming_file.ok:
					with page_loc.open('w', encoding='utf-8') as man_page:
						for line in incoming_file.iter_lines(decode_unicode=True):
							man_page.write(line + '\n')

def install_antlr(version: str = _LATEST, /) -> None:
	'''Install the given version of the ANTLR toolchain.'''
	
	if version == _LATEST:
		query_url = "https://search.maven.org/solrsearch/select?q=a:antlr4-master+g:org.antlr"
		try:
			version = get(query_url, timeout=3).json()['response']['docs'][0]['latestVersion']
		
		except (RequestException, KeyError, IndexError):
			log('\t    fail: Unable not resolve latest version of ANTLR', stderr)
			return
	
	jar_name = f"antlr-{version}-complete.jar"
	destination = USER_BINARY_PATH / jar_name
	
	if destination.exists():
		log(f"\t   error: {jar_name} already exists")
		return
	
	url = f"https://www.antlr.org/download/{jar_name}"
	log(f"\t   fetch: {jar_name}")
	if not _RUNNING_DRY:
		try:
			incoming_file = get(url, timeout=3, stream=True)
			incoming_file.raise_for_status()
		
		except RequestException:
			log(f"\t    fail: Could not get {url}", stderr)
		
		else:
			if incoming_file.ok:
				with destination.open('wb') as jar:
					for chunk in incoming_file.iter_content(chunk_size=256):
						jar.write(chunk)
			
			ensure_python_packages(['antlr4-python3-runtime'])

def install_minikube(version: str = _LATEST, /) -> None:
	'''Install the given version of minikube.'''
	
	fetch_github_binary('kubernetes/minikube', 'minikube', version)
	
	minikube = USER_BINARY_PATH / 'minikube'
	if minikube.exists() and which('kubectl') is None:
		log('\t    link: kubectl to minikube')
		(USER_BINARY_PATH / 'kubectl').symlink_to(minikube)

def install_ghcup(version: str = _LATEST, /) -> None:
	'''Install the given version of ghcup.'''
	
	# pylint: disable=line-too-long(C0301)
	
	repo = 'haskell/ghcup-hs'
	macos_part = 'apple-' if _DISTRO_FAMILY is DistroFamily.MACOS else ''
	pattern = f"{{arch}}-{macos_part}{{host}}-ghcup-" # The '-' at the end is important here.
	fetch_github_binary(repo, 'ghcup', version, pattern=pattern)
	
	# The completion script is not included in the releases.
	comp_loc = ENV_COMP_DIR / '_ghcup'
	log(f"\t   fetch: {comp_loc.stem} from {repo}")
	if not _RUNNING_DRY and not comp_loc.exists():
		url = 'https://raw.githubusercontent.com/haskell/ghcup-hs/refs/heads/master/scripts/shell-completions/zsh'
		try:
			incoming_file = get(url, timeout=3, stream=True)
			incoming_file.raise_for_status()
		
		except RequestException:
			log(f"\t    fail: Could not get {comp_loc.stem}", stderr)
		
		else:
			if incoming_file.ok:
				with comp_loc.open('w', encoding='utf-8') as comp_script:
					for line in incoming_file.iter_lines(decode_unicode=True):
						comp_script.write(line + '\n')

def enable_flatpak() -> None:
	'''Ensure that flathub is registered as a remote and make links to existing applications.'''
	
	if _DISTRO_FAMILY is not DistroFamily.MACOS:
		log('\t  enable: flatpak remote (flathub)')
		if not _RUNNING_DRY:
			remote_cmd = [
				'flatpak',
				'--user',
				'remote-add',
				'--if-not-exists',
				'flathub',
				'https://dl.flathub.org/repo/flathub.flatpakrepo',
			]
			_ = run(remote_cmd, stdout=stdout, check=True)
		
		# ! This feature needs to be reexamined; seems half-baked.
		# ! Not all programs have their name as the last piece and
		# ! 	some are not intended to be run via cli.
		# def link_binaries(root: Path, /) -> None:
		# 	'''Link files (binaries) in the given path to the (convenient) flatpak folder.'''
			
		# 	flatpak_bin_dir = USER_BINARY_PATH / 'flatpak'
		# 	if not flatpak_bin_dir.is_dir():
		# 		log(f"\t makedir: {flatpak_bin_dir}")
		# 		flatpak_bin_dir.mkdir(parents=True, exist_ok=True)
				
		# 	if root.is_dir():
		# 		for app in root.iterdir():
		# 			# The last part of the identifier is usually the application name.
		# 			app_name = app.name.split('.')[-1]
		# 			shortcut = flatpak_bin_dir / app_name
					
		# 			if not shortcut.exists():
		# 				log(f"\t    link: [flatpak] {app_name} -> {app.name}")
		# 				shortcut.symlink_to(app)
					
		# 			elif shortcut.is_symlink():
		# 				log(f"\t    info: [flatpak] Link '{app_name}' -> {app.readlink()}")
					
		# 			elif shortcut.is_file():
		# 				log(f"\t warning: [flatpak] File '{app_name}' already exists")
		
		# log('\t    link: (shortcuts to) flatpak system applications')
		# if not _RUNNING_DRY:
		# 	link_binaries(Path('/var/lib/flatpak/exports/share'))
		
		# log('\t    link: (shortcuts to) flatpak user applications')
		# if not _RUNNING_DRY:
		# 	link_binaries(Path('~/.local/share/flatpak/exports/share').expanduser())

def wrap_script(script_name: str, /) -> None:
	'''Run named script.'''
	
	cmd = ['zsh', str(get_env_dir() / 'scripts' / script_name)]
	log(f"\t     run: {' '.join(cmd)}")
	if not _RUNNING_DRY:
		_ = run(cmd, stdout=stdout, check=True)

def clone_repo(
		repo: str,
		/,
		links: Sequence[tuple[Path | str, Path]] | None = None,
		remote: str = 'github.com',
		*,
		cmds: Sequence[Sequence[str]] | None = None,
	) -> bool:
	'''
	Clone a repo, and link the contents to the right place.
	
	`links` maps source files to the requested destinations.
	The sources may be relative to the cloned repo's root.
	
	`cmds` is a list of commands to run after the clone, and
		will not affect the running of the calling process.
	These commands will only output to stdout.
	'''
	
	from subprocess import CalledProcessError
	
	base_cmd = ['git', '-C']
	repo_dir = ENV_REPOS_DIR / repo.split('/')[1]
	
	if not repo_dir.is_dir():
		clone_params = [str(ENV_REPOS_DIR), 'clone', '--filter=blob:none', f"https://{remote}/{repo}.git"]
		
		log(f"\t   clone: {clone_params[-1]}")
		if not _RUNNING_DRY:
			try:
				_ = run([*base_cmd, *clone_params], stdout=stdout, check=True)
			
			except CalledProcessError:
				# This should not just crash the script.
				return False
	
	base_cmd.append(str(repo_dir))
	
	log(f"\t    pull: changes for {repo_dir}")
	if not _RUNNING_DRY:
		_ = run([*base_cmd, 'config', 'pull.ff', 'only'], stdout=stdout, check=True)
		_ = run([*base_cmd, 'pull'], stdout=stdout, check=True)
	
	for (src, dest) in (links or []):
		dest = dest.resolve() # noqa: PLW2901
		
		if isinstance(src, str):
			src = Path(src) # noqa: PLW2901
		
		if not src.is_absolute():
			src = repo_dir / src # noqa: PLW2901
		
		if dest.exists():
			if dest.is_symlink() and dest.readlink() == src:
				continue
			
			log(f"\t  refuse: {dest} already exists")
			
		else:
			log(f"\t    link: {dest}")
			if not _RUNNING_DRY:
				dest.symlink_to(src)
	
	for cmd in (cmds or []):
		log(f"\t     run: (repo trigger) {' '.join(cmd)}")
		if not _RUNNING_DRY:
			_ = run(cmd, stdout=stdout, check=False)
	
	return True

def change_shell() -> None:
	'''Change the login shell.'''
	
	from pwd import getpwnam # pylint: disable=import-error(E0401) # type: ignore[reportAttributeAccessIssue]
	
	shell_name = '/bin/zsh'
	# Homebrew zsh cannot be used bcs compaudit will never accept the completions.
	# if _DISTRO_FAMILY is DistroFamily.MACOS:
	# 	shell_name = '/opt/homebrew' + shell_name
	
	if getpwnam(_USER).pw_shell != shell_name:
		log(f"\t  change: login shell to {shell_name}")
		if not _RUNNING_DRY:
			_ = run(['chsh', '-s', shell_name, _USER], stdout=stdout, check=True)
			print(f"Login again to make {shell_name} active.")

def make_parser() -> ArgumentParser:
	'''Make and return a suitable cmd line argument parser.'''
	
	from argparse import RawTextHelpFormatter as RTHF
	
	# Make changes to line 3 to change the header of the parser.
	epilog = 'Shell completion for this script is generated on first use of the environment.'
	parser = ArgumentParser(
		description=globals()['__doc__'], epilog=epilog, formatter_class=RTHF, allow_abbrev=False,
	)
	
	quiet_help = 'suppress trace messages'
	parser.add_argument('-q', '--quiet', action='store_true', help=quiet_help, dest='quiet_run')
	
	very_quiet_help = 'suppress all output'
	parser.add_argument('-Q', '--silent', action='store_true', help=very_quiet_help, dest='silent_run')
	
	dry_help = 'dry run; take no action(s)'
	parser.add_argument('-d', '--dry', action='store_true', help=dry_help, dest='dry_run')
	
	direnv_help = f"install direnv (default: {_LATEST})"
	parser.add_argument(
		'--direnv',
		nargs='?',
		const=_LATEST,
		help=direnv_help,
		metavar='DIRENV_VERSION',
		dest='install_direnv',
	)
	
	antlr_help = f"install ANTLR (default: {_LATEST})"
	parser.add_argument(
		'--antlr',
		nargs='?',
		const=_LATEST,
		help=antlr_help,
		metavar='ANTLR_VERSION',
		dest='install_antlr',
	)
	
	minkube_help = f"install minikube (default: {_LATEST})"
	parser.add_argument(
		'--minikube',
		nargs='?',
		const=_LATEST,
		help=minkube_help,
		metavar='KUBE_VERSION',
		dest='install_minikube',
	)
	
	ghcup_help = f"install ghcup (default: {_LATEST})"
	parser.add_argument(
		'--ghcup',
		nargs='?',
		const=_LATEST,
		help=ghcup_help,
		metavar='GHCUP_VERSION',
		dest='install_ghcup',
	)
	
	return parser

def main(arguments: Sequence[str], /) -> int:
	'''Run sequence of steps to setup this host with user permissions.'''
	
	# pylint: disable=global-statement(W0603),too-many-branches(R0912)
	
	from sys import executable as exe
	
	from distro_util import SessionFamily, LibcFamily, distro_name as get_distro_name
	
	global stdout # noqa: PLW0603
	global _RUNNING_DRY # noqa: PLW0603
	global _RUNNING_QUIET # noqa: PLW0603
	
	parser = make_parser()
	args = parser.parse_args(arguments)
	
	if _HOST_SYSTEM == 'Windows':
		return 2
	
	_RUNNING_DRY = args.dry_run
	_RUNNING_QUIET = args.quiet_run
	
	if args.silent_run:
		# pylint: disable=consider-using-with(R1732)
		_RUNNING_QUIET = True
		stdout = Path(devnull).open('a', encoding='utf-8') # noqa: SIM115
	
	# print(args)
	# sys_exit(0)
	
	enable_linger()
	
	correct_home_folder()
	publish_env()
	
	enable_flatpak()
	ensure_python_packages()
	
	if not _HOST_MACHINE.startswith('x86') and _DISTRO_FAMILY is DistroFamily.ALPINE:
		log('\t  refuse: rustc on musl is unable to build some binaries.', stderr)
	
	elif _DISTRO_FAMILY is not DistroFamily.MACOS:
		update_rustup()
		
		wrap_script('install_vivid.sh')
		
		if _DISTRO_FAMILY in (DistroFamily.DEBIAN, DistroFamily.REDHAT):
			pattern = f"{{arch}}-unknown-{{host}}-{LibcFamily.fetch().name.lower()}"
			fetch_github_binary('theryangeary/choose', 'choose', pattern=pattern)
		
		if _DISTRO_FAMILY is DistroFamily.DEBIAN:
			wrap_script('install_delta.sh')
			wrap_script('install_eza.sh')
			wrap_script('install_hexyl.sh')
			
			if get_distro_name() != 'ubuntu':
				wrap_script('install_treesitter.sh')
			
			if SessionFamily.is_running_gui():
				wrap_script('build_alacritty.sh')
	
	if args.install_direnv is not None:
		install_direnv(args.install_direnv)
	
	if args.install_antlr is not None:
		install_antlr(args.install_antlr)
	
	if args.install_minikube is not None:
		install_minikube(args.install_minikube)
	
	if args.install_ghcup is not None:
		install_ghcup(args.install_ghcup)
	
	if _DISTRO_FAMILY in (DistroFamily.REDHAT, DistroFamily.MACOS):
		wrap_script('build_lua.sh')
	
	# Fonts on macOS are handled by Homebrew, if possible.
	if _DISTRO_FAMILY is not DistroFamily.MACOS:
		font_dir = USER_DATA_PATH / 'fonts'
		correct_folder(font_dir)
		clone_repo(
			'adobe-fonts/source-code-pro',
			[('OTF', font_dir / 'source-code-pro')],
			cmds=[['fc-cache', '-f']],
		)
		
		wrap_script('install_cascadia_font.sh')
		
		cmd = [exe, str(get_env_dir() / 'scripts' / 'install_nerd_fonts.py')]
		log(f"\t     run: {' '.join(cmd)}")
		if not _RUNNING_DRY:
			_ = run(cmd, stdout=stdout, check=True)
	
	clone_repo(
		'mfaerevaag/wd',
		[
			('wd.1', MAN1_DIR / 'wd.1'),
			('_wd.sh', ENV_COMP_DIR / '_wd'),
			('wd.sh', USER_BINARY_PATH / 'wd'),
		],
		cmds=[['wd', 'list']],
	)
	# clone_repo('b3nj5m1n/xdg-ninja', [('xdg-ninja.sh', USER_BINARY_PATH / 'xdg-ninja')])
	clone_repo('zsh-users/zsh-syntax-highlighting')
	
	change_shell()
	
	# Remember to configure git user.{name,email}.
	# Remember to create gpg and ssh keys if they are needed.
	# https://docs.gitlab.com/ee/ssh/
	# https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html
	
	# Remember to install 'gtk4-desktop-icons-ng-ding' by smedius in 'Extension Manager' on GNOME.
	
	return 0

if __name__ == '__main__':
	sys_exit(main(argv[1:]))

# vim: set ts=4 sw=0 sts=-1 noet ai sta:

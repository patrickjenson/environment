#!/bin/zsh

setopt ERR_EXIT NO_UNSET PIPE_FAIL

: ${KEY_TYPE:=rsa}
: ${NEWLINE:=$'\n'}

# KEY_TYPE, and KEY_LENGTH may be configured in the environment to change these values.
KEY_NAME="${KEY_NAME:-id_${KEY_TYPE}}"

cd "${HOME}/.ssh"
if [[ -r "${KEY_NAME}" ]]; then
	echo "\n\tKey '${KEY_NAME}' already exists."
	exit 1
fi

typeset -a ssh_params
ssh_params=()
ssh_params+=('-t' "${KEY_TYPE}")
ssh_params+=('-b' "${KEY_LENGTH:-3072}")
ssh_params+=('-f' "${KEY_NAME}")

# Set KEY_PASS to anything to have the agent ask for a passphrase.
if [[ 0 -eq "${+KEY_PASS}" ]]; then
	ssh_params+=('-N' '')
fi

# Set KEY_COMMENT to the empty string to skip this query.
if [[ 0 -eq "${+KEY_COMMENT}" ]]; then
	comment_prompt="Enter the comment for this key, or ^D to omit it.${NEWLINE}> "
	vared -ce -p "${comment_prompt}" KEY_COMMENT || true
	echo ''
fi
if [[ -n "${KEY_COMMENT:-}" ]]; then
	ssh_params+=('-C' "${KEY_COMMENT}")
fi

ssh-keygen -q "${ssh_params[@]}"
cat "${KEY_NAME}.pub" >> authorized_keys
ssh-add "${KEY_NAME}" &>/dev/null

exit 0

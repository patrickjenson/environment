#!/bin/bash

set -euo pipefail

cd "${env_repos_dir}"

version='5.1.5'
repo_dir="lua-${version}"

if [[ ! -d "${repo_dir}" ]]; then
	file_name="lua-${version}.tar.gz"
	curl -fsSLO --output-dir '/tmp' "https://www.lua.org/ftp/${file_name}"
	tar -xzf "/tmp/${file_name}"
fi

cd "${repo_dir}"
env_repo="$(dirname $(dirname $(realpath ${0})))"
git apply "${env_repo}/macos/lua51.patch" 2>/dev/null || true

typeset -l platform="$(uname -s)"
if [[ 'darwin' == "${platform}" ]]; then
	platform='macosx'
fi

target='target'
if [[ ! -d "${target}" ]]; then
	echo "Building recipe: ${platform}"
	
	# The install path is relative to 'src/'.
	make echo "${platform}" test install "INSTALL_TOP=../${target}"
fi

mkdir -p "${env_man_dir}/man1"
for prg in lua{,c}; do
	dest_prg="${prg}${version:0:3}"
	
	# 'lib/liblua.a' and 'include/lua*.h{,pp}' also exist, but there is no reason to put them elsewhere.
	
	if [[ ! -L "${XDG_BIN_HOME}/${dest_prg}" ]]; then
		ln -s "$(pwd)/${target}/bin/${prg}" "${XDG_BIN_HOME}/${dest_prg}"
	fi
	
	if [[ ! -L "${env_man_dir}/man1/${dest_prg}.1" ]]; then
		ln -s "$(pwd)/${target}/man/man1/${prg}.1" "${env_man_dir}/man1/${dest_prg}.1"
	fi
done

exit 0

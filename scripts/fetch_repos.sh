#!/bin/bash

set -euo pipefail

: "${repos_dir:=${HOME}/repos}"

if [[ ! -d "${repos_dir}" ]]; then
	echo "${repos_dir} does not exist." >&2
	exit 1
fi

for repo in "$(command ls ${repos_dir})"; do
	(
		cd "${repo}"
		git fetch --all &>/dev/null
	)
done
unset repo

exit 0

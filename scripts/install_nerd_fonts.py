#!/usr/bin/env python3

'''A simple script to download and install specific Nerd Fonts from the latest release.'''

from os import environ, umask
from pathlib import Path
from subprocess import DEVNULL, run
from stat import S_IRWXG, S_IRWXO
from sys import exit as sys_exit
from tempfile import TemporaryDirectory

from requests import get, RequestException

# pylint: disable=unnecessary-lambda-assignment(C3001)

fonts = [
	['NerdFontsSymbolsOnly.zip', False, ''], # -> 'Symbols Nerd Font [ Mono]'
	# # The regular face for this is 'Book' and does not seem to install properly.
	# ['LiberationMono.zip', False, ''], # -> 'LiterationMono Nerd Font [ Mono]'
	['SourceCodePro.zip', False, ''], # -> 'SauceCodePro Nerd Font [ Mono]'
]

API_ROOT = 'https://api.github.com/repos/ryanoasis/nerd-fonts/releases'
rel_id = get(f"{API_ROOT}/latest", timeout=3).json()['id']
asset_url = f"{API_ROOT}/{rel_id}/assets"
for page in range(1, 3):
	asset_list = get(asset_url, params={'per_page': 100, 'page': page}, timeout=3).json()
	for asset in asset_list:
		url = asset['browser_download_url']
		for (index, (name, found, _)) in enumerate(fonts):
			if not found and url.endswith(name):
				fonts[index] = [name, True, url]
				break

_ = umask(S_IRWXG | S_IRWXO)
with TemporaryDirectory(ignore_cleanup_errors=True) as font_dl_dir:
	font_dl_path = Path(font_dl_dir)
	font_dest_dir = Path(environ.get('XDG_DATA_HOME', '~/.local/share')).expanduser() / 'fonts'
	font_dest_dir.mkdir(parents=True, exist_ok=True)
	
	get_name = lambda font_pkg: font_pkg.split('.', maxsplit=1)[0]
	font_names = (get_name(name) for (name, _, _) in fonts)
	longest = len(max(font_names, key=len))
	FOUND_FONTS = 0
	for (name, found, url) in fonts:
		if not found:
			print(f"    Error: Unable to find {name}.")
			continue
		
		font_name = name.split('.', maxsplit=1)[0] # type: ignore[attr-defined]
		dest_dir = font_dest_dir / font_name
		if dest_dir.is_dir():
			print(f"     Info: {font_name:>{longest}} is already installed at {dest_dir}.")
			continue
		
		dl_name = font_dl_path / name # type: ignore[operator]
		try:
			incoming_file = get(url, timeout=3, stream=True)
			incoming_file.raise_for_status()
		
		except RequestException:
			print(f"    Error: Could not download {name}.")
		
		else:
			if incoming_file.ok:
				with dl_name.open('wb') as font_file:
					for chunk in incoming_file.iter_content(chunk_size=256):
						font_file.write(chunk)
				
				dest_dir.mkdir(parents=True)
				_ = run(['unzip', '-d', dest_dir, dl_name], stdout=DEVNULL, check=True)
				for curr_path in dest_dir.iterdir():
					curr_file = str(curr_path) # pylint: disable=invalid-name(C0103)
					if curr_file.find('Windows') != -1 or curr_file.startswith('LICENSE') or curr_file == 'readme.md':
						(dest_dir / curr_file).unlink()
				
				FOUND_FONTS += 1
				print(f"     Info: {font_name:>{longest}} is now installed at {dest_dir}.")
	
	if FOUND_FONTS > 0:
		_ = run(['fc-cache', '-f'], check=True)

sys_exit(0)

# vim: set ts=4 sw=0 sts=-1 noet ai sta:

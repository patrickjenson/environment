#!/bin/bash

set -euo pipefail

# Manage a Docker (rootless) install on Linux.
# Docker for macOS needs to be configured in the Desktop client.

# This script is written for Debian/RedHat based systems using SystemD; YMMV.

# Alpine is unable to use rootless Docker for now; use rootful instead.

if [[ 0 -lt ${#} && ( '-h' == "${1}" || '--help' == "${1}" ) ]]; then
	echo "\tusage: [sudo | doas] $(basename ${0}) [-h | remove | USER [USER ...]]" >&2
	exit 1
fi

if [[ 'root' == "$(id -nu)" ]]; then
	if [[ 0 -lt ${#} && 'remove' == "${1}" ]]; then
		systemctl disable --now containerd
		systemctl disable --now docker.socket
		systemctl disable --now docker
		if [[ -n "$(whence apt)" ]]; then
			apt purge -y docker-{ce{,-cli,-rootless-extras},compose-plugin} containerd.io || true
			apt -y autoremove
		elif [[ -n "$(whence dnf)" ]]; then
			dnf remove -y docker-{ce{,-cli,-rootless-extras},compose-plugin} containerd.io || true
			dnf -y autoremove
		fi
		rm -rdf /{var/lib,run}/{docker,containerd} 2>/dev/null || true
		echo "\tReboot to finish docker removal..."
	
	else
		if [[ -z "$(whence -p docker)" ]]; then
			# See: https://get.docker.com/rootless for updates on the rootless setup process.
			if [[ -n "$(whence apt)" ]]; then
				apt install -y lsb-release uidmap dbus-user-session slirp4netns iptables
				if [[ -n "$(lsb_release -is | command grep -P '^(?:De|Rasp)bian$')" ]]; then
					apt install -y fuse-overlayfs
				fi
			elif [[ -n "$(whence dnf)" ]]; then
				dnf install -y redhat-lsb-core shadow-utils fuse-overlayfs dbus-daemon slirp4netns iptables
			fi
			# The below script is not zsh compatible.
			curl -fsSL https://get.docker.com | bash -s >/dev/null
		
		else
			if [[ -n "$(whence apt)" ]]; then
				apt -y full-upgrade
			elif [[ -n "$(whence dnf)" ]]; then
				dnf -y upgrade
			fi
		fi
		systemctl enable containerd
		if [[ 0 -eq ${#} || 'root' != "${1}" ]]; then
			systemctl disable --now docker.socket
			systemctl disable --now docker
		fi
		
		in_a_script=0
		if [[ 0 -lt ${#} && '--script' == "${1}" ]]; then
			in_a_script=1
			shift
		fi
		
		for user in "${SUDO_USER:-${DOAS_USER:-}}" ${@:+"${@}"}; do
			if [[ -n "$(getent passwd ${user})" ]]; then
				loginctl enable-linger "${user}"
				if [[ ! -e /etc/subuid || -z "$(command grep ^${user} /etc/subuid)" ]]; then
					echo "${user}:165536:65536" >> /etc/subuid
				fi
				if [[ ! -e /etc/subgid || -z "$(command grep ^${user} /etc/subgid)" ]]; then
					echo "${user}:165536:65536" >> /etc/subgid
				fi
			fi
		done
		if [[ 0 -eq "${in_a_script}" ]]; then
			echo "\tEach user needs to run this script to setup rootless containers for themselves."
		fi
	fi

elif [[ 0 -lt ${#} && 'remove' == "${1}" ]]; then
	dockerd-rootless-setuptool.sh uninstall &>/dev/null
	docker context use default
	echo "\tLogout to finish disabling rootless docker for this account."

elif [[ -n "$(whence -p docker)" ]]; then
	if [[ -z "$(systemctl --user list-unit-files | command grep docker)" ]]; then
		dockerd-rootless-setuptool.sh install &>/dev/null
	fi
	loginctl enable-linger
	systemctl --user enable docker
	if [[ ! -d "${HOME}/.docker" ]]; then
		# Docker will store credentials and configuration information here.
		mkdir -m 0750 "${HOME}/.docker"
	fi
	docker context use rootless
	if [[ 0 -eq ${#} || '--script' != "${1}" ]]; then
		echo "\tLogout to finish enabling rootless docker for this account."
	fi

else
	echo "\tRun this script as root first..." >&2
	exit 1
fi

exit 0

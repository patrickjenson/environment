#!/bin/bash

set -euo pipefail

project='alacritty'

export XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
export CARGO_HOME="${CARGO_HOME:-${XDG_DATA_HOME}/cargo}"

cargo install "${project}${CRATE_VERSION:+@${CRATE_VERSION}}"

crate=$(find "${CARGO_HOME}/registry/src/index.crates.io-"* -type d -name "${project}-${CRATE_VERSION:-*}" | sort -nr | head -n1)
cd "${crate}/extra"

if [[ -n "$(whence scdoc)" ]]; then
	env_man_dir="${env_man_dir:-${XDG_DATA_HOME}/man}"
	mkdir -p "${env_man_dir}"/man{1,5}
	
	scdoc < 'man/alacritty.1.scd' | gzip > "${env_man_dir}/man1/alacritty.1.gz"
	scdoc < 'man/alacritty-msg.1.scd' | gzip > "${env_man_dir}/man1/alacritty-msg.1.gz"
	
	scdoc < 'man/alacritty.5.scd' | gzip > "${env_man_dir}/man5/alacritty.5.gz"
	scdoc < 'man/alacritty-bindings.5.scd' | gzip > "${env_man_dir}/man5/alacritty-bindings.5.gz"
else
	echo "WARNING: Alacritty manpages are not available due to missing program 'scdoc'." >&2
fi

mkdir -p "${XDG_DATA_HOME}/applications"
ln -sf "$(pwd)/linux/Alacritty.desktop" "${XDG_DATA_HOME}/applications/Alacritty.desktop"
# This would help notify the system of the new change, but it only works when run as SUPER_USER.
# update-desktop-database

mkdir -p "${XDG_DATA_HOME}/icons"
ln -sf "$(pwd)/logo/alacritty-term.svg" "${XDG_DATA_HOME}/icons/Alacritty.svg"

typeset -x TERMINFO="${HOME}/.terminfo"
mkdir -p "${TERMINFO}"
tic -xe 'alacritty' "$(pwd)/alacritty.info"

exit 0

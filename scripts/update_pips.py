#!/usr/bin/env python3

'''
A simple script to update all pips in the current environment.
Warning: This may update a dependency that needs to stay an older version.
'''

from json import loads
from subprocess import run
from sys import executable as exe, exit as sys_exit
from venv import CORE_VENV_DEPS

pips: list[str] = []
exceptions: list[str] = []

pip_proc = run(
	(exe, '-m', 'pip', 'list', '--format', 'json'),
	capture_output=True,
	check=True,
	encoding='utf-8',
)
for pkg in loads(pip_proc.stdout): # <list[dict[str{name, version}, str]]>
	name = pkg['name']
	if name not in CORE_VENV_DEPS and name not in exceptions:
		pips.append(name)

for pkg_list in (CORE_VENV_DEPS, pips):
	if len(pkg_list) > 0:
		# ~ The output is quite messy; clean up?
		_ = run(
			(exe, '-m', 'pip', 'install', '--upgrade', *pkg_list),
			check=True,
		)

sys_exit(0)

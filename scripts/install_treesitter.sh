#!/bin/bash

set -euo pipefail

repo='tree-sitter/tree-sitter'
url="https://api.github.com/repos/${repo}/releases/latest"

raw_assets="$(curl -fsSL ${url})"
latest_assets=$(jq -c '[.assets[].browser_download_url]' <<< "${raw_assets}")

case "$(uname -s)" in
	Linux)
		host='linux'
		;;
	Darwin)
		host='macos'
		;;
	*)
		host='windows'
		;;
esac

case "$(uname -m)" in
	aarch64 | arm64)
		arch='arm64'
		;;
	arm*)
		arch='arm'
		;;
	x86_64 | amd64)
		arch='x64'
		;;
	*)
		arch='dud'
		;;
esac

resource=$(
	jq -r --arg host "${host}" --arg arch "${arch}" \
	'.[] | select(contains($host) and contains($arch) and endswith("gz"))' \
	<<< "${latest_assets}"
)

bin_dir="${XDG_BIN_HOME:-${HOME}/.local/bin}"
mkdir -p "${bin_dir}"
curl -fsSL "${resource}" | gunzip > "${bin_dir}/tree-sitter"

exit 0

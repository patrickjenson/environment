#!/bin/bash

set -euo pipefail

env_repo="$(dirname $(dirname $(realpath ${0})))"
chmod u+x ${env_repo}/env_{root,user}.py "${env_repo}/scripts"/*

if [[ 0 -eq "${EUID}" ]]; then
	progs=('zsh' 'git' 'curl')
	
	distro_name="$(uname -s)"
	if [[ 'Darwin' != "${distro_name}" ]]; then
		distro_name="$(cat /etc/*-release | sed -rn 's%^ID="?([^"]+)"?$%\1%p' | head -n1)"
	fi
	
	# Currently known and unsupported distros:
	# 	Zorin,
	# 	Manjaro,
	# 	Void,
	
	# Install the basic programs needed to deploy the environment.
	case "${distro_name}" in
		Darwin)
			# python3(comes with pip, venv)
			export HOMEBREW_NO_ANALYTICS=1
			export HOMEBREW_NO_EMOJI=1
			export HOMEBREW_NO_ENV_HINTS=1
			# Neither zsh, nor curl should be installed with Homebrew;
			# 	zsh can't be used bcs of compaudit complaining about site-functions not being owned by (USER | root),
			# 	curl does not need to be installed bcs macOS comes with a functional copy.
			# Homebrew decided to not provide packages for some python libraries anymore.
			# `requests` is needed to bootstrap the rest of this environment and does not have any non-python dependencies.
			# Allowing pip to potentially break the system packages is not good, but there is very limited danger
			# 	due to the included workaround for PEP 668 that will be used for package installation thereafter.
			sudo -HEu _brew /bin/zsh -c '
				/opt/homebrew/bin/brew install --quiet git python3
				/opt/homebrew/bin/git config --system safe.directory /opt/homebrew
				/opt/homebrew/bin/pip3 install requests --break-system-packages
			'
			;;
		debian | raspbian | ubuntu)
			apt update
			# -pip provides python3
			apt install -y "${progs[@]}" python3-{pip,requests,venv}
			;;
		fedora)
			# -pip provides python3(comes with venv)
			dnf install -y "${progs[@]}" python3-{pip,requests}
			;;
		alpine)
			# -pip provides python3(comes with venv)
			sed -ri '/edge/! s%^#(.*/community)$%\1%' /etc/apk/repositories
			apk update
			apk add "${progs[@]}" py3-{pip,requests}
			;;
		*)
			echo -e "\n\tUnsupported distro: '${distro_name}'.\n" >&2
			exit 1
			;;
	esac
fi

exit 0

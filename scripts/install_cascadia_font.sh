#!/bin/bash

set -euo pipefail

typeset -x XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
font_name='cascadia'
install_dir="${XDG_DATA_HOME}/fonts/${font_name}"

mkdir -p "${install_dir:h}"
if [[ ! -d "${install_dir}" ]]; then
	tmp_dir="$(mktemp -d)"
	pushd -q "${tmp_dir}"
	curl -fsSLO "$(
		curl -fsSL 'https://api.github.com/repos/microsoft/cascadia-code/releases/latest' | \
		sed -rn 's%^ *"browser_download_url": "(.+\.zip)",?$%\1%p' | \
		head -n1
	)"
	unzip ./*.zip >/dev/null
	mkdir "${install_dir}"
	mv ./otf/static/CascadiaMono* "${install_dir}"
	popd -q
	rm -rf "${tmp_dir}"
fi

fc-cache -f
echo "=>  $(fc-list | command grep -i ${font_name} | wc -l) ${font_name} fonts are available."

exit 0

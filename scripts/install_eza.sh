#!/bin/bash

set -euo pipefail

repo='eza-community/eza'
url="https://api.github.com/repos/${repo}/releases/latest"

raw_assets="$(curl -fsSL ${url})"
latest_assets=$(jq -c '[.assets[].browser_download_url]' <<< "${raw_assets}")

# Binary
host_triple="$(rustc -vV | sed -rn 's%^host: (.*)$%\1%p')"
bin_resource=$(
	jq -r --arg triple "${host_triple}" \
	'.[] | select(contains($triple) and endswith("gz"))' \
	<<< "${latest_assets}"
)

bin_dir="${XDG_BIN_HOME:-${HOME}/.local/bin}"
mkdir -p "${bin_dir}"
curl -fsSL "${bin_resource}" | tar -xzC "${bin_dir}" --no-anchored --strip-components=1 'eza'

# Shell completion
comp_resource=$(jq -r '.[] | select(contains("completions") and endswith("gz"))' <<< "${latest_assets}")

comp_dir="${env_completion_dir:-${HOME}/.zsh}"
mkdir -p "${comp_dir}"
curl -fsSL "${comp_resource}" | tar -xzC "${comp_dir}" --no-anchored --strip-components=2 '_eza'

# Manpages
man_resource=$(jq -r '.[] | select(contains("man") and endswith("gz"))' <<< "${latest_assets}")
tmp_file="$(mktemp)"
curl -fsSL "${man_resource}" > "${tmp_file}"

man_dir="${XDG_DATA_HOME:-${HOME}/.local/share}/man"

mkdir -p "${man_dir}/man1"
tar -xzC "${man_dir}/man1" --no-anchored --strip-components=2 'eza.1' < "${tmp_file}"

mkdir -p "${man_dir}/man5"
tar -xzC "${man_dir}/man5" --no-anchored --strip-components=2 --wildcards 'eza_colors*.5' < "${tmp_file}"

rm "${tmp_file}"

exit 0

#!/bin/zsh

setopt ERR_EXIT NO_UNSET PIPE_FAIL

# See: https://www.gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html
# See: https://www.linode.com/docs/guides/gpg-key-for-ssh-authentication/#serve-your-gpg-key-instead-of-an-ssh-key
# See: https://www.g-loaded.eu/2010/11/01/change-expiration-date-gpg-key/

# See: https://superuser.com/questions/360507/are-gpg-and-ssh-keys-interchangable
# 	gpg --export-ssh-key <key_id>[!]

# See: https://gist.github.com/chrisroos/1205934
# 	gpg --armor --export <key_id> > public.key
# 	gpg --armor --export-secret-key <key_id> > private.key
# 	gpg --export-ownertrust > ownertrust.txt
# 	gpg --import private.key # A private(secret) key contains the public key also.
# 	gpg --import-ownertrust ownertrust.txt

# To show the long and short fingerprints of keys:
# 	gpg --list-keys --keyid-format=long

# To fetch a public key:
# 	gpg --keyserver host.com --recv-key FINGERPRINT

# To publish a key:
# 	gpg --keyserver host.com --send-key FINGERPRINT

# To generate a revocation certificate for a key:
# This certificate can be published to make the associated public keys no longer trusted.
# 	gpg --output revoke.asc --gen-revoke FINGERPRINT

: ${NEWLINE:=$'\n'}
echo ''

# GPG_NAME and GPG_EMAIL may be set in the environment before running to skip these queries.

if [[ -z "${GPG_NAME:-}" ]]; then
	name_prompt="Enter your real- or user-name.${NEWLINE}> "
	vared -c -p "${name_prompt}" GPG_NAME
	echo ''
fi

if [[ -z "${GPG_EMAIL:-}" ]]; then
	email_prompt="Enter the email address for this key.${NEWLINE}> "
	vared -c -p "${email_prompt}" GPG_EMAIL
	echo ''
fi

# Set KEY_COMMENT to the empty string to skip this query.
if [[ 0 -eq "${+KEY_COMMENT}" ]]; then
	comment_prompt="Enter the comment for this key, or ^D to omit it.${NEWLINE}> "
	vared -ce -p "${comment_prompt}" KEY_COMMENT || true
	echo ''
fi

# Set KEY_PASS to anything to have the agent ask for a passphrase.
if [[ 0 -eq "${+KEY_PASS}" ]]; then
	key_passphrase="Passphrase: ''"
fi

# KEY_TYPE, KEY_LENGTH, and GPG_EXPIRE may be configured in the environment to change these values.
# GPG_EXPIRE should be of the form 'YYYY-MM-DD' and keys will expire at 00:00 local time on that day.

(
gpg --batch --generate-key 2>&1 <<-EOF
	# %dry-run
	${key_passphrase:+%no-protection}
	Key-Type: ${KEY_TYPE:-rsa}
	Key-Length: ${KEY_LENGTH:-3072}
	Key-Usage: sign,encrypt,auth
	Name-Real: ${GPG_NAME}
	Name-Email: ${GPG_EMAIL}
	Expire-Date: ${GPG_EXPIRE:-0}
	${key_passphrase:-}
	${KEY_COMMENT:+Name-Comment: "${KEY_COMMENT}"}
EOF
) | sed -rn 's%^.* ([[:alnum:]]{16}) .*$%Key \1 created.%p'

exit 0

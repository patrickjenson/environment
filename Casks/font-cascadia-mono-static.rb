cask "font-cascadia-mono-static" do
  version "2404.23"
  sha256 "a911410626c0e09d03fa3fdda827188fda96607df50fecc3c5fee5906e33251b"

  url "https://github.com/microsoft/cascadia-code/releases/download/v#{version}/CascadiaCode-#{version}.zip"
  name "Cascadia Mono - Static"
  desc "Version of Cascadia Code without ligatures"
  homepage "https://github.com/microsoft/cascadia-code"

  livecheck do
    url :url
    strategy :github_latest
  end

  font "otf/static/CascadiaMono-Bold.otf"
  font "otf/static/CascadiaMono-BoldItalic.otf"
  font "otf/static/CascadiaMono-ExtraLight.otf"
  font "otf/static/CascadiaMono-ExtraLightItalic.otf"
  font "otf/static/CascadiaMono-Italic.otf"
  font "otf/static/CascadiaMono-Light.otf"
  font "otf/static/CascadiaMono-LightItalic.otf"
  font "otf/static/CascadiaMono-Regular.otf"
  font "otf/static/CascadiaMono-SemiBold.otf"
  font "otf/static/CascadiaMono-SemiBoldItalic.otf"
  font "otf/static/CascadiaMono-SemiLight.otf"
  font "otf/static/CascadiaMono-SemiLightItalic.otf"

  # No zap stanza required
end

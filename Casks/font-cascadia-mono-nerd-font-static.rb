cask "font-cascadia-mono-nerd-font-static" do
  version "2404.23"
  sha256 "a911410626c0e09d03fa3fdda827188fda96607df50fecc3c5fee5906e33251b"

  url "https://github.com/microsoft/cascadia-code/releases/download/v#{version}/CascadiaCode-#{version}.zip"
  name "Cascadia Mono Nerd Font - Static"
  desc "Version of Cascadia Code without ligatures and with embedded Powerline and NerdFont symbols"
  homepage "https://github.com/microsoft/cascadia-code"

  livecheck do
    url :url
    strategy :github_latest
  end

  font "otf/static/CascadiaMonoNF-Bold.otf"
  font "otf/static/CascadiaMonoNF-BoldItalic.otf"
  font "otf/static/CascadiaMonoNF-ExtraLight.otf"
  font "otf/static/CascadiaMonoNF-ExtraLightItalic.otf"
  font "otf/static/CascadiaMonoNF-Italic.otf"
  font "otf/static/CascadiaMonoNF-Light.otf"
  font "otf/static/CascadiaMonoNF-LightItalic.otf"
  font "otf/static/CascadiaMonoNF-Regular.otf"
  font "otf/static/CascadiaMonoNF-SemiBold.otf"
  font "otf/static/CascadiaMonoNF-SemiBoldItalic.otf"
  font "otf/static/CascadiaMonoNF-SemiLight.otf"
  font "otf/static/CascadiaMonoNF-SemiLightItalic.otf"

  # No zap stanza required
end

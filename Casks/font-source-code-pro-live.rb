cask "font-source-code-pro-live" do
  # This project cannot be fetched as a zip bcs Homebrew cannot handle the way releases are tagged.

  version :latest
  sha256 :no_check

  url "https://github.com/adobe-fonts/source-code-pro.git",
      verified:  "github.com/adobe-fonts/source-code-pro",
      branch:    "release",
      only_path: "OTF"
  name "Source Code Pro"
  desc "Source Code Pro is a set of OpenType fonts that have been designed to work well in user interface (UI) environments."
  homepage "https://adobe-fonts.github.io/source-code-pro"

  font "SourceCodePro-Black.otf"
  font "SourceCodePro-BlackIt.otf"
  font "SourceCodePro-Bold.otf"
  font "SourceCodePro-BoldIt.otf"
  font "SourceCodePro-ExtraLight.otf"
  font "SourceCodePro-ExtraLightIt.otf"
  font "SourceCodePro-It.otf"
  font "SourceCodePro-Light.otf"
  font "SourceCodePro-LightIt.otf"
  font "SourceCodePro-Medium.otf"
  font "SourceCodePro-MediumIt.otf"
  font "SourceCodePro-Regular.otf"
  font "SourceCodePro-SemiBold.otf"
  font "SourceCodePro-SemiBoldIt.otf"

  # No zap stanza required
end

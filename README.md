# environment
An environment including, but not limited to terminal, shell, editor, and system configuration files.

This also includes a management system to fetch the latest updates and re-publish the changes to an installation.

Please review _all_ script code before running it on your system.
You (the user/administrator) are responsible for changes to your system, not me.

## Usage
If, _and only if_, using macOS:
> Only Apple Silicon is supported; not Intel.
```sh
% curl -fsSLO --output-dir /tmp https://gitlab.com/patrickjenson/environment/-/raw/main/macos/setup.sh

% sudo /bin/zsh /tmp/setup.sh

% eval "$(/opt/homebrew/bin/brew shellenv zsh)"

# Continue with `All platforms` section...

# When using the environment for the first time, run
% update_completions
# to make the Homebrew completion scripts available; and
% update_brew_fonts
# to make the Homebrew fonts available to other programs.
```

For Linux _only_:
```sh
# Some distros (Eg. Ubuntu) don't come with Git installed by default.
# This can be tested with:
$ [ -n "$(which git)" ] && echo 'Git is installed.' || echo 'Git is missing.'

# Git must be available to get the rest of this environment,
# 	and can usually be installed with somthing like:
$ sudo apt install -y git

# Continue with `All platforms` section...

# Logout + Login after finishing the below steps to make the environment active.
```

All platforms:
```sh
# Enter password as needed when running scripts in this repo.

$ git clone https://gitlab.com/patrickjenson/environment.git/

$ cd environment

$ [sudo | doas] [bash | zsh] ./scripts/enable.sh

$ (sudo | doas) ./env_root.py

$ ./env_user.py
```

## Caveats
In general this environment only supports the most recent (and possibly the previous)
stable release(s) of the OS and tools.

This framework only supports distros that have a _single_ primary/common/default init system.

- ( `SystemD` ^ `OpenRC` ^ `RunIt` ^ `LaunchD`)

Distros, like `Artix`, that have several init options available are too complex to easily support.

The same can be said of distros that are built/constructed by the user.

- Examples: `Gentoo`, and `Linux From Scratch`

`Raspbian / Raspberry Pi OS` does not see the same testing frequency as other distros.

That configuration should be considered best effort only, though it should work as expected.

## ISOs

| Distro  | Architecture | Use Case | Default DE | Link(s)                                                                   | Notes |
| ------  | ------------ | -------- | ---------- | ------------------------------------------------------------------------- | ----- |
| Ubuntu  | aarch64      | Server   | TTY        | [Download](https://ubuntu.com/download/server/arm/)                       |       |
| Ubuntu  | x86_64       | Server   | TTY        | [Download](https://ubuntu.com/download/server/)                           |       |
| Ubuntu  | aarch64      | Desktop  | GNOME      | [Download (Daily)](https://cdimages.ubuntu.com/noble/daily-live/current/) | LTS releases _only_. See note below. |
| Ubuntu  | x86_64       | Desktop  | GNOME      | [Download](https://ubuntu.com/download/desktop/)                          |       |
| Fedora  | all          | Server   | TTY        | [Download](https://fedoraproject.org/server/download/)                    |       |
| Fedora  | all          | Desktop  | GNOME      | [Download](https://fedoraproject.org/workstation/download/), or<br>[Download (Archive)](https://archives.fedoraproject.org/pub/fedora/linux/releases/), or<br>[Download (Netinstaller)](https://alt.fedoraproject.org/) | F40+ has issues with making the regular aarch64 ISO, <br>but the Everything (net-instaler) ISO works just fine. |
| Alpine  | all          | -        | TTY        | [Download](https://alpinelinux.org/downloads/)                            | No images come with a GUI by default. |

> Note:\
> While it is possible to install a GUI on Ubuntu Server (via `apt install ubuntu-gnome-desktop`), this is not recommended due to how long that process takes.\
> This is important because there seems to be some inconsistency in the availability of an `arm64` Desktop ISO, even for an LTS release.

> Additional information about (un-)supported distros can be found in `scripts/enable.sh`.

## macOS Files
macOS is mostly compatible out-of-the-box with one notable exception, a package manager.

Currently `Homebrew` is the package manager supported by this project.

`macos` has the script to setup and wrap access to Homebrew, and `Cask` has font configurations different than the defaults.

## Windows Files
The files in `windows` are intended to be a minimal configuration for an environment like `Git for Windows`, or any other `Bash`-only situation.

Other config files from this repo can be added to these, perhaps with some (minor) modifications, to enable other functionality.

If a file is usable/meaningful to a Windows environment, it will have a comment near the top with its required location.

It is highly recomended to have Git install a [Windows Terminal](https://github.com/microsoft/terminal) profile, and use that instead of `Git Bash`.

```sh
# This will also be useful when working on *nix repos on Windows.
# Either run this command or use the `.gitconfig` provided in this repo,
# 	and uncomment the relevant line.

$ git config --global core.fileMode false
```

## Contributing
I will modify/add/remove code from here as I see fit.

PRs are welcome, but may be refused for any reason.

It may also be a while before I review the PR.

## License
Licensed under `Apache License 2.0` by Patrick Jenson

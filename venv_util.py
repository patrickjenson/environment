#!/usr/bin/env python3

'''Utilities for helping to manage virtual environments.'''

from __future__ import annotations
from argparse import ArgumentParser
from pathlib import Path
from platform import system
from sys import argv, exit as sys_exit
from typing import TYPE_CHECKING
from venv import EnvBuilder

if TYPE_CHECKING:
	from collections.abc import Sequence
	from types import SimpleNamespace
	from typing import Final

__all__ = ['CurrentInterpreterError', 'GlobalEnv']

class CurrentInterpreterError(RuntimeError):
	'''Raised when trying to upgrade the current interpreter or virtualenv.'''
	
	MESSAGE: Final[str] = 'Cannot upgrade the in-use interpreter or virtualenv.'
	
	@staticmethod
	def hint(script_file: str, /, script_args: Sequence[str] | None = None) -> str:
		'''Get a suggestion of what to do about this error.'''
		
		message = "Try running: `$(whence -a python3 | sed -n \'2p\') {}{}`."
		arguments = ''
		
		if script_args is not None and len(script_args) != 0:
			arguments = ' '.join(['', *script_args])
		
		return message.format(Path(script_file).name, arguments)
	
	def __init__(self, script_file: str, /, script_args: Sequence[str] | None = None) -> None:
		super().__init__(f"{self.MESSAGE}\n{self.hint(script_file, script_args)}")

class GlobalEnv(EnvBuilder):
	'''EnvBuilder subclass to create a global venv in response to PEP 668.'''
	
	DEFAULT_ENV_NAME: Final[str] = 'global'
	DEFAULT_ENV_DIR: Final[str] = 'venvs'
	ENV_DIR_VAR: Final[str] = 'venvs_dir'
	
	def __init__(self, /, **kwargs) -> None:
		'''Create a Builder with minimal options.'''
		
		# `symlinks=True` prevents this venv from working-around PEP 668.
		super().__init__(**kwargs, with_pip=True, upgrade_deps=True)
		
		self._extras: list[str] = []
		self._extras_only: bool = False
		self._with_scripts: bool = False
	
	@classmethod
	def get_packages(cls, /) -> list[str]:
		'''Fetch the list of packages to add to the venv.'''
		
		packages = [
			# 'adafruit-ampy',
			'distro',
			# 'esptool',
			# 'ffmpy',
			# 'gpg-lite',
			# 'httpx[cli]',
			'icecream',
			# 'jedi', # included with python-lsp-server
			'mypy',
			'Pygments',
			# 'pexpect',
			'ptpython',
			# 'pipdeptree',
			# 'psutil',
			'pylint',
			'pylsp-mypy',
			'pylsp-rope',
			'pynvim',
			# 'pypugjs',
			# 'python-dotenv[cli]',
			'python-lsp-server[mccabe,pylint,rope,yapf]',
			'radon',
			'readchar',
			'requests',
			# 'rich',
			# 'rich-cli',
			'ruff',
			'shtab',
			# 'sympy',
			# 'tldr',
			# 'tmuxp',
			# 'tqdm',
			# 'trash-cli',
			'types-requests', # this will silence an error from mypy
			# 'unimport', # useful but, requires a unicode terminal (emoji); PR?
			# 'yapf', # included with python-lsp-server
			# 'zstd',
		]
		
		if system() != 'Windows':
			packages.extend([
				'sh',
			])
		
		return packages
	
	@classmethod
	def venv_path(cls, venv_location: str | None = None, /) -> Path:
		'''Get the full path to the specified venv.'''
		
		from os import environ
		
		venv_loc = venv_location or environ.get(cls.ENV_DIR_VAR)
		
		if venv_loc is not None:
			venv_path = Path(venv_loc).expanduser()
		else:
			venv_path = Path.home() / cls.DEFAULT_ENV_DIR
		
		return venv_path
	
	def build(
		self,
		env_name: str = DEFAULT_ENV_NAME,
		extra_pkgs: Sequence[str] | None = None,
		*,
		with_scripts: bool = False,
		venv_location: str | None = None,
	) -> None:
		'''Create a venv with modified arguments.'''
		
		# pylint: disable=too-many-arguments(R0913)
		
		from sys import executable
		
		self._with_scripts = with_scripts
		if extra_pkgs is not None and len(extra_pkgs) > 0:
			self._extras_only = True
			self._extras = list(extra_pkgs)
		
		venv_path = self.venv_path(venv_location)
		
		if Path(executable).is_relative_to(venv_path):
			given_args = []
			
			if env_name != self.DEFAULT_ENV_NAME:
				given_args.extend(['-n', env_name])
			
			if venv_location is not None:
				given_args.extend(['-l', venv_location])
			
			if with_scripts:
				given_args.append('-s')
			
			if extra_pkgs is not None:
				given_args.extend(extra_pkgs)
			
			raise CurrentInterpreterError(__file__, given_args)
		
		self.create(venv_path / env_name)
	
	def setup_scripts(self, context: SimpleNamespace) -> None:
		'''Prevent the `activate` scripts from being copied into the venv by default.'''
		
		if self._with_scripts:
			super().setup_scripts(context)
	
	def upgrade_dependencies(self, context: SimpleNamespace) -> None:
		'''Verify/update packages for the venv.'''
		
		from subprocess import STDOUT
		from venv import CORE_VENV_DEPS
		
		options = ['-m', 'pip', 'install', '--upgrade', '--no-warn-script-location']
		
		super()._call_new_python(context, *options, *CORE_VENV_DEPS, stderr=STDOUT) # type: ignore[misc]
		
		packages = self.get_packages()
		
		if self._extras_only:
			packages = self._extras
		
		elif len(self._extras) > 0:
			packages.extend(self._extras)
		
		super()._call_new_python(context, *options, *packages, stderr=STDOUT) # type: ignore[misc]

def make_parser() -> ArgumentParser:
	'''Make and return a suitable cmd line argument parser.'''
	
	from argparse import RawTextHelpFormatter as RTHF, SUPPRESS
	from textwrap import dedent
	
	parser = ArgumentParser(
		description='A tool to cleanly create/update a venv while accounting for PEP 668.',
		epilog=dedent(f"""\
		Without `-e` a standard set of packages will be installed/updated,
			in addition to any listed in `extra_pkgs`.
		
		The location of the venv will be determined by the first of:
			- argument to `-l` (may need to be quoted)
			- the environment variable `{GlobalEnv.ENV_DIR_VAR}`
			- the location `~/{GlobalEnv.DEFAULT_ENV_DIR}`
		
		Package names with optional features need to be quoted.
		"""),
		formatter_class=RTHF, allow_abbrev=False,
	)
	
	parser.add_argument(
		'-n', '--name', default=SUPPRESS, metavar='NAME', dest='env_name',
		help=f"the name of the venv to create/update; default: '{GlobalEnv.DEFAULT_ENV_NAME}'",
	)
	parser.add_argument(
		'-l', '--location', default=SUPPRESS, metavar='LOCATION', dest='venv_location',
		help='the location to put the venv; see note',
	)
	parser.add_argument(
		'-s', '--with-scripts', action='store_true', default=SUPPRESS,
		help='include the `activate` scripts in the venv',
	)
	parser.add_argument(
		'extra_pkgs', nargs='*', default=SUPPRESS,
		help='other packages to include in the venv; see note',
	)
	
	return parser

if __name__ == '__main__':
	input_args = argv[1:]
	args = make_parser().parse_args(input_args)
	
	try:
		GlobalEnv().build(**vars(args))
	
	except CurrentInterpreterError as e:
		print(f"\n\t{e.MESSAGE}\n\t{e.hint(__file__, input_args)}")
		sys_exit(1)
